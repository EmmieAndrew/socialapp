<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('users', 'UserAPIController');
Route::post('login','UserAPIController@login');
Route::post('socialLogin','UserAPIController@socialLogin');
Route::post('register','UserAPIController@register');
Route::post('preRegister','UserAPIController@preRegister');
Route::post('forgotPassword','UserAPIController@forgotPassword');
Route::post('changePassword','UserAPIController@changePassword');
Route::post('getOwnProfile', 'UserAPIController@getOwnProfile');
Route::post('editProfile', 'UserAPIController@editProfile');
Route::post('verifyOtp', 'UserAPIController@verifyOtp');
Route::post('resendOtp', 'UserAPIController@resendOtp');
Route::post('setPassword', 'UserAPIController@setPassword');
Route::get('accountDetail', 'UserAPIController@accountDetail');
Route::post('addActivity', 'UserAPIController@addActivity');
Route::post('follow', 'UserAPIController@follow');
Route::get('suggessions', 'UserAPIController@suggessions');
Route::post('blockedLists', 'UserAPIController@blockedLists');
Route::post('deleteAccount', 'UserAPIController@deleteAccount');
Route::post('search', 'UserAPIController@search');
Route::post('viewUser', 'UserAPIController@viewUser');
Route::post('getBuddies', 'UserAPIController@getBuddies');
Route::post('getNotifications', 'UserAPIController@getNotifications');
#Visa
// Route::resource('visas', 'VisaAPIController');
// Route::get('visaDetail', 'VisaAPIController@visaDetail');
// Route::get('visaStatus', 'VisaAPIController@visaStatus');
// Route::post('visaPayment', 'VisaAPIController@visaPayment');

#Banners
Route::resource('banners', 'BannerAPIController');
Route::get('getBanners', 'BannerAPIController@getBanners');

#Query
Route::resource('queries', 'QueryAPIController');

#Reports
Route::resource('reports', 'ReportAPIController');

#Pages
Route::resource('pages', 'PagesAPIController');
Route::post('createPage', 'PagesAPIController@createPage');
Route::post('createPost', 'PagesAPIController@createPost');
Route::post('homeListing', 'PagesAPIController@homeListing');
Route::post('getStorys', 'PagesAPIController@getStorys');
Route::post('shareStory', 'PagesAPIController@shareStory');
Route::post('myPages', 'PagesAPIController@myPages');
Route::post('getPage', 'PagesAPIController@getPage');
Route::post('likeComments', 'PagesAPIController@likeComments');
Route::post('createInterest', 'PagesAPIController@createInterest');
Route::post('getPost', 'PagesAPIController@getPost');
Route::post('menuPages', 'PagesAPIController@menuPages');
Route::post('sharePost', 'PagesAPIController@sharePost');
Route::post('randomPosts', 'PagesAPIController@randomPosts');
Route::post('randomVideos', 'PagesAPIController@randomVideos');
Route::post('editPage', 'PagesAPIController@editPage');
Route::post('deletePost', 'PagesAPIController@deletePost');
Route::post('deleteStory', 'PagesAPIController@deleteStory');
Route::post('leaderBoard', 'PagesAPIController@leaderBoard');

#Interest
Route::resource('interests', 'InterestAPIController');
Route::get('getInterests', 'CategoriesAPIController@getInterests');

#Language
Route::resource('languages', 'LanguageAPIController');
Route::get('getLanguages', 'LanguageAPIController@getLanguages');

Route::resource('advertisements', 'AdvertisementAPIController');

Route::resource('leaderboards', 'LeaderboardAPIController');

Route::resource('posts', 'PostAPIController');

Route::resource('categories', 'CategoriesAPIController');