<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index');
// Route::get('/home', 'HomeController@index')->middleware('verified');

Route::resource('users', 'UserController');
Route::get('userban/{id}','UserController@userBanUnban');
Route::get('searchuser','UserController@searchuser');

Route::resource('visas', 'VisaController');
// Route::get('notify/{id}','VisaController@notify');
// Route::get('ban_unban/{id}','VisaController@ban_unban');

Route::resource('banners', 'BannerController');

Route::resource('queries', 'QueryController');

Route::resource('reports', 'ReportController');

Route::resource('pages', 'PagesController');
Route::get('pageban/{id}','PagesController@pageBanUnban');
Route::get('searchpage','PagesController@searchpage');

Route::resource('interests', 'InterestController');

Route::resource('languages', 'LanguageController');

Route::resource('advertisements', 'AdvertisementController');

Route::resource('leaderboards', 'LeaderboardController');

Route::resource('posts', 'PostController');

Route::resource('categories', 'CategoriesController');