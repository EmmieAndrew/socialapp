<?php
 
namespace App\Traits;

use App\Models\User;
// use App\Models\Device;
use App\Models\Pages;
use App\Models\Action;
use App\Models\LikeComment;
use App\Models\Notification;
use Illuminate\Http\Request;

trait CommonFunctionTrait {

    public function storeNotification($data) 
    {
        Notification::insert(
                [
                    'user_id' => $data['user_id'],
                    'object_id' => $data['object_id'],
                    'message' => $data['message'],
                    'act' => $data['act']
                ]
            );
        return 1;
    }

    public function randomPassword($length = 8) 
    {
      $characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function followings($type,$id) {
        if ($type == 'P') {
            $getPageFollowings = Action::where('user_id',$id)
            ->where('object_type','PAGE')
            ->where('status','followed')
            ->get();
            return $getPageFollowings;
        }elseif ($type == 'U') {
            $getUserFollowers = Action::where('user_id',$id)
            ->where('object_type','USER')
            ->where('status','followed')
            ->get();
            return $getUserFollowers;
        }
    }

    public function followers($type,$id) {
        if ($type == 'P') {
            $getPageFollowers = Action::where('object_id',$id)
            ->where('object_type','PAGE')
            ->where('status','followed')
            ->get();
            return $getPageFollowers;
        }elseif ($type == 'U') {
            $getUserFollowers = Action::where('object_id',$id)
            ->where('object_type','USER')
            ->where('status','followed')
            ->get();
            return $getUserFollowers;
        }
    }

    public function blockedUsers($id) {
            $getBlockedUsers = Action::where('user_id',$id)
            ->where('object_type','USER')
            ->where('status','blocked')
            ->get();
            return $getBlockedUsers;
    }
 
    public function rewardSnowflakes($amount, $reason, $id) {
        Snowflake_reward::insert(
                [
                    'user_id' => $id,
                    'amount' => $amount, 
                    'rewarded_spent' => 'rewarded',
                    'status' => 'confirmed',
                    'reward_for' => $reason,
                ]
            );
    }

    public function generateRandomString($length = 6) 
    {
      $characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function thumblikesCount($postid){
        $getthumbLikes = LikeComment::where('post_id',$postid)
        // ->where('isliked','!=','')
        ->where('isliked','1')
        ->get();
        return $getthumbLikes;
    }

    public function heartlikesCount($postid){
        $getHeartLikes = LikeComment::where('post_id',$postid)
        // ->where('isliked','!=','')
        ->where('isliked','2')
        ->get();
        return $getHeartLikes;
    }

    public function smilelikesCount($postid){
        $getSmileLikes = LikeComment::where('post_id',$postid)
        // ->where('isliked','!=','')
        ->where('isliked','3')
        ->get();
        return $getSmileLikes;
    }

    public function flowerlikesCount($postid){
        $getFlowerLikes = LikeComment::where('post_id',$postid)
        // ->where('isliked','!=','')
        ->where('isliked','4')
        ->get();
        return $getFlowerLikes;
    }

    // public function changeFlagStatus($id, $flag){
    //     $flag = \App\Flag::where('user_id', '=', $id)->where('flag', '=', $flag)->update(['flag_status' => 1]);
    // }

	// public function send_notification($id, $message){
 //        //decides which device type
 //    	$devices = \App\Device::where('user_id', $id)->get();
 //        foreach($devices as $device){
 //            if($device->device_type == "A"){
 //                $this->android_send_notification($device->device_id, $message);
 //              }
 //            else if($device->device_type == "I"){
 //                $this->iphone_send_notifications($device->device_id, $message);
 //              }
 //        }
 //    }

    public function android_send_notification($deviceIdArr, $message,$user_id){
        // dd($user_id);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => array($deviceIdArr),
            'data' => array(
                 "message" => $message,
                 "Notification_id" => "1",
                 "user_id" => $user_id
             )
        );
        if(!defined('GOOGLE_API_KEY')){
            $GOOGLE_API_KEY = 'AAAAWuE3HvU:APA91bGTsxwrohjASm1GjCsRZMk0VK89W0HQESYLYLHQCijtnF_SiRo8RRqQmdYfABVEFugu8SZHteNDYVAwe8ZiqCClQVtxGE-f80jTRCPmCxGhy7M8qvKy5QA9sSSA8GIbHi3X_MR2';
        }
        $headers = array(
            'Authorization: key='.$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        // print_r($result);die();
    }

    public function iphone_send_notifications($deviceIdArr, $message,$user_id){
        $result='';
        $body['aps'] = array(
            'alert' => [
             "body" => $message,
             "user_id" => $user_id,
             "title" => 'Notification'
            ],
            'noti_for'=> $message,
            'sound' => 'default',
            'badge' => 0
        );
        $payload = json_encode($body);
        $ctx = stream_context_create();
        $passphrase = '';
        // stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/pem/nameOfFile.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 4, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    
        if($fp){
            $message = chr(0) . pack('n', 32) . pack('H*', $deviceIdArr[0]) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $message, strlen($message)); 
            fclose($fp);
        }
        return $result;
    }

    /*
	   * Business Permission
    */

    // public function businessPermission(Request $request ,$business_id,$userExist) {
    // 	$checkPermission = Permission::where('entity_id',$business_id)
    //     ->where('entity_type','Business')
    //     ->where('user_id',$userExist)
    //     ->where(['permission_type'=>'ADM'])
    //     ->first();
    // 	if ($checkPermission) {
    // 		return 1;
    // 	}else{
    // 		return 0;
    // 	}
    // }
}