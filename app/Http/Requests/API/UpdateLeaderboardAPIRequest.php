<?php

namespace App\Http\Requests\API;

use App\Models\Leaderboard;
use InfyOm\Generator\Request\APIRequest;

class UpdateLeaderboardAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Leaderboard::$rules;
        
        return $rules;
    }
}
