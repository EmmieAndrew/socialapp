<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLeaderboardRequest;
use App\Http\Requests\UpdateLeaderboardRequest;
use App\Repositories\LeaderboardRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class LeaderboardController extends AppBaseController
{
    /** @var  LeaderboardRepository */
    private $leaderboardRepository;

    public function __construct(LeaderboardRepository $leaderboardRepo)
    {
        $this->leaderboardRepository = $leaderboardRepo;
    }

    /**
     * Display a listing of the Leaderboard.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $leaderboards = $this->leaderboardRepository->all();

        return view('leaderboards.index')
            ->with('leaderboards', $leaderboards);
    }

    /**
     * Show the form for creating a new Leaderboard.
     *
     * @return Response
     */
    public function create()
    {
        return view('leaderboards.create');
    }

    /**
     * Store a newly created Leaderboard in storage.
     *
     * @param CreateLeaderboardRequest $request
     *
     * @return Response
     */
    public function store(CreateLeaderboardRequest $request)
    {
        $input = $request->all();

        $leaderboard = $this->leaderboardRepository->create($input);

        Flash::success('Leaderboard saved successfully.');

        return redirect(route('leaderboards.index'));
    }

    /**
     * Display the specified Leaderboard.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            Flash::error('Leaderboard not found');

            return redirect(route('leaderboards.index'));
        }

        return view('leaderboards.show')->with('leaderboard', $leaderboard);
    }

    /**
     * Show the form for editing the specified Leaderboard.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            Flash::error('Leaderboard not found');

            return redirect(route('leaderboards.index'));
        }

        return view('leaderboards.edit')->with('leaderboard', $leaderboard);
    }

    /**
     * Update the specified Leaderboard in storage.
     *
     * @param int $id
     * @param UpdateLeaderboardRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLeaderboardRequest $request)
    {
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            Flash::error('Leaderboard not found');

            return redirect(route('leaderboards.index'));
        }

        $leaderboard = $this->leaderboardRepository->update($request->all(), $id);

        Flash::success('Leaderboard updated successfully.');

        return redirect(route('leaderboards.index'));
    }

    /**
     * Remove the specified Leaderboard from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            Flash::error('Leaderboard not found');

            return redirect(route('leaderboards.index'));
        }

        $this->leaderboardRepository->delete($id);

        Flash::success('Leaderboard deleted successfully.');

        return redirect(route('leaderboards.index'));
    }
}
