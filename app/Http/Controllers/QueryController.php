<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQueryRequest;
use App\Http\Requests\UpdateQueryRequest;
use App\Repositories\QueryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class QueryController extends AppBaseController
{
    /** @var  QueryRepository */
    private $queryRepository;

    public function __construct(QueryRepository $queryRepo)
    {
        $this->queryRepository = $queryRepo;
    }

    /**
     * Display a listing of the Query.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $queries = $this->queryRepository->all();

        return view('queries.index')
            ->with('queries', $queries);
    }

    /**
     * Show the form for creating a new Query.
     *
     * @return Response
     */
    public function create()
    {
        return view('queries.create');
    }

    /**
     * Store a newly created Query in storage.
     *
     * @param CreateQueryRequest $request
     *
     * @return Response
     */
    public function store(CreateQueryRequest $request)
    {
        $input = $request->all();

        $query = $this->queryRepository->create($input);

        Flash::success('Query saved successfully.');

        return redirect(route('queries.index'));
    }

    /**
     * Display the specified Query.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            Flash::error('Query not found');

            return redirect(route('queries.index'));
        }

        return view('queries.show')->with('query', $query);
    }

    /**
     * Show the form for editing the specified Query.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            Flash::error('Query not found');

            return redirect(route('queries.index'));
        }

        return view('queries.edit')->with('query', $query);
    }

    /**
     * Update the specified Query in storage.
     *
     * @param int $id
     * @param UpdateQueryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQueryRequest $request)
    {
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            Flash::error('Query not found');

            return redirect(route('queries.index'));
        }

        $query = $this->queryRepository->update($request->all(), $id);

        Flash::success('Query updated successfully.');

        return redirect(route('queries.index'));
    }

    /**
     * Remove the specified Query from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            Flash::error('Query not found');

            return redirect(route('queries.index'));
        }

        $this->queryRepository->delete($id);

        Flash::success('Query deleted successfully.');

        return redirect(route('queries.index'));
    }
}
