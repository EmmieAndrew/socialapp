<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePagesRequest;
use App\Http\Requests\UpdatePagesRequest;
use App\Repositories\PagesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Pages;
use Flash;
use Response;
use DB;

class PagesController extends AppBaseController
{
    /** @var  PagesRepository */
    private $pagesRepository;

    public function __construct(PagesRepository $pagesRepo)
    {
        $this->pagesRepository = $pagesRepo;
    }

    /**
     * Display a listing of the Pages.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pages = Pages::orderBy('id','DESC')->paginate(10);

        return view('pages.index')
            ->with('pages', $pages);
    }

    public function pageBanUnban($page_id)
    {
        $getPage = Pages::where('id',$page_id)->first();
        if ($getPage->status == '1') {
            $update = Pages::where('id','=',$page_id)->update(['status' => '3']);
            $pages = Pages::orderBy('id','DESC')->paginate(10);
            Flash::success('Page Banned.');
            return view('pages.index')
            ->with('pages', $pages);
        }elseif ($getPage->status == '3') {
            $update = Pages::where('id','=',$page_id)->update(['status' => '1']);
            $pages = Pages::orderBy('id','DESC')->paginate(10);
            Flash::success('Page Unbanned.');
            return view('pages.index')
            ->with('pages', $pages);
        }
    }

    /**
     * Show the form for creating a new Pages.
     *
     * @return Response
     */
    public function create()
    {
        $users = DB::table("users")->pluck("name","id");
        return view('pages.create',compact('users'));
        // return view('pages.create');
    }

    /**
     * Store a newly created Pages in storage.
     *
     * @param CreatePagesRequest $request
     *
     * @return Response
     */
    public function store(CreatePagesRequest $request)
    {
        $input = $request->all();

        $pages = $this->pagesRepository->create($input);

        Flash::success('Pages saved successfully.');

        return redirect(route('pages.index'));
    }

    /**
     * Display the specified Pages.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error('Pages not found');

            return redirect(route('pages.index'));
        }

        return view('pages.show')->with('pages', $pages);
    }

    /**
     * Show the form for editing the specified Pages.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error('Pages not found');

            return redirect(route('pages.index'));
        }

        return view('pages.edit')->with('pages', $pages);
    }

    /**
     * Update the specified Pages in storage.
     *
     * @param int $id
     * @param UpdatePagesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagesRequest $request)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error('Pages not found');

            return redirect(route('pages.index'));
        }

        $pages = $this->pagesRepository->update($request->all(), $id);

        Flash::success('Pages updated successfully.');

        return redirect(route('pages.index'));
    }

    /**
     * Remove the specified Pages from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pages = $this->pagesRepository->find($id);

        if (empty($pages)) {
            Flash::error('Pages not found');

            return redirect(route('pages.index'));
        }

        $this->pagesRepository->delete($id);

        Flash::success('Pages deleted successfully.');

        return redirect(route('pages.index'));
    }

    public function searchpage(Request $request)
    {
        $search = $request->get('search');
        $pages = Pages::where('name','like','%'.$search.'%')
        ->orderBy('id','DESC')->paginate(10);
        if($pages){
            return view('pages.index')->with('pages', $pages);
        }
    }
}
