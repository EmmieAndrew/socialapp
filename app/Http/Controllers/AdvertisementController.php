<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdvertisementRequest;
use App\Http\Requests\UpdateAdvertisementRequest;
use App\Repositories\AdvertisementRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use Flash;
use Response;
use DB;

class AdvertisementController extends AppBaseController
{
    /** @var  AdvertisementRepository */
    private $advertisementRepository;

    public function __construct(AdvertisementRepository $advertisementRepo)
    {
        $this->advertisementRepository = $advertisementRepo;
    }

    /**
     * Display a listing of the Advertisement.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $advertisements = Advertisement::join('pages', 'pages.id', '=', 'advertisements.page_id')
        // ->orderBy('id', 'DESC')
        // ->select('pages.page_title as pageTitle','advertisements.*')
        // ->paginate(5);
        // return view('resorts.index')->with('resorts', $resorts);
        $advertisements = $this->advertisementRepository->paginate(5);
        return view('advertisements.index')
            ->with('advertisements', $advertisements);
    }

    /**
     * Show the form for creating a new Advertisement.
     *
     * @return Response
     */
    public function create()
    {
        $pages = DB::table("pages")->pluck("page_title","id");
        return view('advertisements.create',compact('pages'));
        // return view('advertisements.create');
    }

    /**
     * Store a newly created Advertisement in storage.
     *
     * @param CreateAdvertisementRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // $input = $request->all();
        $pageId = $request->input('page');
        $post_desc = $request->input('post_desc');
        $page_title = $request->input('page_title');
        $period = $request->input('period');

        if($request->hasFile('media')){
            $media = time().$request->media->getClientOriginalName();
            $request->media->move(public_path('/advertisement') . '/', $media);
        }else{
            $media = '';
        }
        $resortdetail = [
            'page_id' => $pageId,
            'post_desc' => $post_desc?$post_desc:'',
            'page_title' => $page_title?$page_title:'',
            'media' => $media,
            'period' => $period,
            'created_at'=>date('Y-m-d h:i:s'),
        ];
        $rdetail =  Advertisement::insertGetId($resortdetail);
        // $advertisement = $this->advertisementRepository->create($input);

        Flash::success('Advertisement saved successfully.');

        return redirect(route('advertisements.index'));
    }

    /**
     * Display the specified Advertisement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        return view('advertisements.show')->with('advertisement', $advertisement);
    }

    /**
     * Show the form for editing the specified Advertisement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        return view('advertisements.edit')->with('advertisement', $advertisement);
    }

    /**
     * Update the specified Advertisement in storage.
     *
     * @param int $id
     * @param UpdateAdvertisementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdvertisementRequest $request)
    {
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        $advertisement = $this->advertisementRepository->update($request->all(), $id);

        Flash::success('Advertisement updated successfully.');

        return redirect(route('advertisements.index'));
    }

    /**
     * Remove the specified Advertisement from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        $this->advertisementRepository->delete($id);

        Flash::success('Advertisement deleted successfully.');

        return redirect(route('advertisements.index'));
    }
}
