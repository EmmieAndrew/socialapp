<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisaRequest;
use App\Http\Requests\UpdateVisaRequest;
use App\Repositories\VisaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Visa_detail;
use App\Models\Visa;
use App\Models\Log;
use Flash;
use Response;

class VisaController extends AppBaseController
{
    /** @var  VisaRepository */
    private $visaRepository;

    public function __construct(VisaRepository $visaRepo)
    {
        $this->visaRepository = $visaRepo;
    }

    /**
     * Display a listing of the Visa.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $visas = Visa::with('visa_detail')->paginate(5);
        // $imgs = explode(",", $visas['media']);
        // $mediaArray = [];
        // foreach ($imgs as $med) {
        //     $media = url('/upload_documents',$med);
        //     $medi['media'] = $media;
        //     $mediaArray[] = $medi;
        // }
        // print_r(json_encode($visas));die();
        // $visas = Visa::join('visa_details', 'visa_details.customer_id', '=', 'visa_applications.id')
        //     ->orderBy('visa_applications.id','desc')
        //     ->select('visa_details.name as username','visa_details.DOB as dob','visa_details.passport_no as passport_no','visa_details.flight_booked as flight_booked','visa_details.accomodation_booked as accomodation_booked','visa_details.passanger_count as passanger_count','visa_details.kids as kids','visa_applications.*')->paginate(10);
            return view('visas.index')->with('visas', $visas);
    }

    /**
     * Show the form for creating a new Visa.
     *
     * @return Response
     */
    public function create()
    {
        return view('visas.create');
    }

    /**
     * Store a newly created Visa in storage.
     *
     * @param CreateVisaRequest $request
     *
     * @return Response
     */
    public function store(CreateVisaRequest $request)
    {
        $input = $request->all();

        $visa = $this->visaRepository->create($input);

        Flash::success('Visa saved successfully.');

        return redirect(route('visas.index'));
    }

    /**
     * Display the specified Visa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visa = Visa::with('visa_detail')->find($id);
        // $visa = $this->visaRepository->find($id);
        // print_r(json_encode($visa));die();
        if (empty($visa)) {
            Flash::error('Visa not found');

            return redirect(route('visas.index'));
        }

        return view('visas.show')->with('visa', $visa);
    }

    public function notify($visa_id)
    {
        // dd($visa_id);
        $visa = Visa_detail::where('visa_id',$visa_id)->first()->toArray();

        if (empty($visa)) {
            Flash::error('somwthing went wrong');

            return redirect(route('visas.index'));
        }else{
            // print_r($visa['email']);die();
            $name = $visa['name'];
            $email = $visa['email'];
            $title = 'Notifiaction Email for Incomplete Visa Documents';
            $content = 'Notifiaction Email for Incomplete Visa Documents';

            \Mail::send('contactUsMail', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) {
              $message->to('sankhyan.amit@gmail.com')->subject('Contact Us Email');
                  });
            /*saving log in db*/
            $logData = [
              'name'=>$name,
              'email'=>$email,
              'subject'=>$title,
              'content'=>$content,
              'created_at'=>date('Y-m-d h:i:s'),
            ];
            $logs = Log::insert($logData);
            // $to_name = 'RECEIVER_NAME';
            // $to_email = 'RECEIVER_EMAIL_ADDRESS';
            // $data = array('name'=>'Ogbonna Vitalis'('sender_name'), 'body' => 'A test mail');
            // Mail::send('contactUsMail', $data, function($message) use ($to_name, $to_email) {
            // $message->to($to_email, $to_name)
            // ->subject('Laravel Test Mail');
            // $message->from('SENDER_EMAIL_ADDRESS’,’Test Mail'   );
            // });

            Flash::success('Notification sent successfully.');

            return redirect(route('visas.index'));
        }
    }

    public function ban_unban($id)
    {
        dd("thanks");
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            Flash::error('Visa not found');

            return redirect(route('visas.index'));
        }

        return view('visas.show')->with('visa', $visa);
    }

    /**
     * Show the form for editing the specified Visa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            Flash::error('Visa not found');

            return redirect(route('visas.index'));
        }

        return view('visas.edit')->with('visa', $visa);
    }

    /**
     * Update the specified Visa in storage.
     *
     * @param int $id
     * @param UpdateVisaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisaRequest $request)
    {
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            Flash::error('Visa not found');

            return redirect(route('visas.index'));
        }

        $visa = $this->visaRepository->update($request->all(), $id);

        Flash::success('Visa updated successfully.');

        return redirect(route('visas.index'));
    }

    /**
     * Remove the specified Visa from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            Flash::error('Visa not found');

            return redirect(route('visas.index'));
        }

        $this->visaRepository->delete($id);

        Flash::success('Visa deleted successfully.');

        return redirect(route('visas.index'));
    }
}
