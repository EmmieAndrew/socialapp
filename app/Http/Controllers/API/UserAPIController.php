<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Models\Pages;
use App\Models\Activities;
use App\Models\Post;
use App\Models\Action;
use App\Models\Language;
use App\Models\Interest;
use App\Models\Share;
use App\Models\LikeComment;
use App\Models\Notification;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use Hash;
use Crypt;
use Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use SendsPasswordResetEmails;
    use CommonFunctionTrait;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $existingUser = User::where('email',$request->email)
            ->where('ban','0')
            ->first();
            if($existingUser){
                $ispage = Pages::where('user_id',$existingUser->id)->first();
                if ($ispage) {
                    $isPageExist = 'yes';
                }else{
                    $isPageExist = 'no';
                }
                if(Hash::check($request->password , $existingUser['password'])){
                    return response()->json([
                        'status' => '1',
                        'message'=>'Logged in successfully', 
                        'user_id'=>(String)$existingUser->id, 
                        'name'=>$existingUser->name,
                        'username'=>$existingUser->username,
                        'email'=>$existingUser->email,
                        'token'=>$existingUser->remember_token,
                        'password_set'=>$existingUser->password_set,
                        'isPageExist'=>$isPageExist,
                    ], $this->successStatus);
                }else {
                  return response()->json(['message'=>'Email or Password is incorrect','status'=>'0'], $this->successStatus);
                }
            }else {
                return response()->json(['message'=>'User not found','status'=>'0'], $this->successStatus);
            }
        }
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'email' => 'bail|required|email|unique:users,email', 
            'dob' => 'required',
            'name' => 'required',
            'phone_no' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $newUser =  new \App\Models\User;
            $newUser->username = $request->username;
            $newUser->name = $request->name;
            $newUser->email = $request->email; 
            $newUser->phone_no = $request->phone_no;
            $newUser->dob = $request->dob;
            $token = (string)mt_rand(100,1000000);
            $token_en = Crypt::encrypt($token);
            $newUser->remember_token = $token_en;
            $newUser->device_id = $request->device_id;
            $newUser->device_type = $request->device_type;
            $newUser->save();
            // $sendotp = $this->sendOtp($newUser->email);
            $ispage = Pages::where('user_id',$newUser->id)->first();
            if ($ispage) {
                $isPageExist = 'yes';
            }else{
                $isPageExist = 'no';
            }
            $pass = User::where('id',$newUser->id)->first();
            $passstatus = $pass->password_set;
            return response()->json([
                'status'=>'1',
                'message'=>'User Registered successfully',
                'user_id'=>(String)$newUser->id,
                'token'=>$newUser->remember_token,
                'password_set'=>$passstatus,
                'isPageExist'=>$isPageExist,
                // 'otp'=>(String)$sendotp,
            ], $this->successStatus);
        }
    }

    public function sendOtp($email){
        $otp = rand(1000,9999);
        $user = User::where('email','=',$email)->update(['otp' => $otp]);
        $mailotp = $this->otpMail($email,$otp);
        return $otp;
    }

    public function preRegister(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'phone_no' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $sendotp = $this->sendOtp($request->email);
            $sendPhoneotp = $this->otpSms($request->phone_no,$sendotp);
            return response()->json([
                'status'=>'1',
                'message'=>'otp sent Successfully',
                'otp'=>(String)$sendotp,
                'email'=>$request->email,
                'phone_no'=>$request->phone_no
            ], $this->successStatus);
        }
    }

    public function otpSms($mobilenumber,$otp){
        $apikey="c7a3cc2d-5ecf-11eb-8153-0200cd936042";
        // $otp=rand(4001,9001);
        if($mobilenumber){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://2factor.in/API/V1/$apikey/SMS/$mobilenumber/$otp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if($response){
                // return json_encode(array("statusCode"=>200,"Otp"=>$otp),JSON_FORCE_OBJECT);
            }else{
                return json_encode(array("statusCode"=>5001,"valid"=>false,'Message'=>'internal server error'),JSON_FORCE_OBJECT);
            }
        }
    }

    public function otpMail($email,$otp){
        $name = 'Dear '.$email.' ';
        $email = $email;
        $title = 'OTP for Email Verification';
        $content = 'Here is the otp of your email varification : '.$otp.'';
        // $msg = [
        //     "name" => $name,
        //     "content" => $content
        // ];
        // Mail::send('mail', $msg, function($message) use ($email) {
        //     $message->to($email);
        //     $message->subject('SocialApp');
        // });
        \Mail::send('contactUsMail', [
            'name' => $name, 
            'email' => $email, 
            'title' => $title,
            'content' => $content
        ], function ($message) use ($email) {
            $message->to($email)->subject('Verification Mail');
        });
    }

    public function verifyOtp(Request $request){
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExist = User::where('id', $request->user_id)->first();
            if($userExist){
                $verify = User::where(['otp' => $request->otp,'email'=> $userExist->email])->first();
                if ($verify) {
                    $user = User::where('id', $request->user_id)->update(['email_verified_at' => date('Y-m-d')]);
                    return response()->json([
                        'status'=>'1',
                        'message'=>'User Verified Successfully',
                        'password_set'=>$userExist->password_set,
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'Otp does not match'
                    ], $this->badrequest);
                }
            }else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'User not found'
                ], $this->badrequest);
            }
        }
    }

    public function setPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExist = User::where('id', $request->user_id)
            // ->where('email_verified_at','!=','Null')
            ->first();
            if($userExist){
                $password = Hash::make($request->password);
                $updatePass = User::where('id', $request->user_id)->update(['password' => $password, 
                'password_set' => 1]);
                if ($updatePass) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Account Created Successfully'
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'Something went wrong'
                    ], $this->badrequest);
                }
            }else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'Please verify your account first'
                ], $this->badrequest);
            }
        }
    }

    public function resendOtp(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            // $userExist = User::where('email', $request->email)->first();
            // if($userExist){
                $sendotp = $this->sendOtp($request->email);
                return response()->json([
                        'status'=>'1',
                        'message'=>'otp sent Successfully',
                        'otp'=>(String)$sendotp
                    ], $this->successStatus);
            // }else{
            //     return response()->json([
            //         'status'=>'0',
            //         'message'=>'User not found'
            //     ], $this->badrequest);
            // }
        }
    }

    public function socialLogin(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'social_id' => 'required',
            'username' =>'required',
            'image' => 'required', #url of image from gmail or fb
            'email' =>'required',
            'login_type' =>'required' #G OR F
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $userExist = User::where('social_id',$saveArray['social_id'])->first();
            if($userExist){
                // $checkEmail = User::where('email',$saveArray['email'])->first();
                // if ($checkEmail) {
                //     return response()->json([
                //         'message'=>'Email Already Exists!!',
                //         'status'=>'1',
                //         'remember_token' => $checkEmail['remember_token'],
                //         'user_id'=>(String)$checkEmail['id'],
                //     ], $this->successStatus);
                // }
                // $checkUsername = User::where('username',$saveArray['username'])->first();
                // if ($checkUsername) {
                //     return response()->json([
                //         'message'=>'Username Already Exists!!',
                //         'status'=>'1',
                //         'remember_token' => $checkUsername['remember_token'],
                //         'user_id'=>(String)$checkUsername['id'],
                //     ], $this->successStatus);
                // }
                $updateData = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:$userExist['device_id'],
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:$userExist['device_type'],
                    'login_type'=>$saveArray['login_type']?$saveArray['login_type']:$userExist['login_type'],
                    'social_id'=>$saveArray['social_id']?$saveArray['social_id']:$userExist['social_id'],
                    'name' => $saveArray['name']?$saveArray['name']:$userExist['name'],
                    // 'username' => $saveArray['username']?$saveArray['username']:$userExist['username'],
                    // 'email' => $saveArray['email']?$saveArray['email']:$userExist['email'],
                    'image'=>$saveArray['image']?$saveArray['image']:$userExist['photo'],
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = User::where('social_id',$saveArray['social_id'])->update($updateData);
                if ($update) {
                    return response()->json([
                        'message'=>'login successfully',
                        'status'=>'1',
                        'user_id'=>(String)$userExist['id'],
                        'token'=>$userExist->remember_token,
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'login Failed. Please try again later.',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }else{
                $checkEmail = User::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json([
                        'message'=>'Email Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkEmail['remember_token'],
                        'user_id'=>(String)$checkEmail['id'],
                    ], $this->successStatus);
                }
                $checkUsername = User::where('username',$saveArray['username'])->first();
                if ($checkUsername) {
                    return response()->json([
                        'message'=>'Username Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkUsername['remember_token'],
                        'user_id'=>(String)$checkUsername['id'],
                    ], $this->successStatus);
                }
                $user = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:'',
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:'',
                    'social_id'=>$saveArray['social_id'],
                    'login_type'=>$saveArray['login_type'],
                    'username' => $saveArray['username'],
                    'name' => $saveArray['name'],
                    'phone_no' => '',
                    'password'=>'',
                    'email' => $saveArray['email'],
                    'image'=>$saveArray['image'],
                    'remember_token' => '',
                    'created_at' => date('Y-m-d h:i:s')
                ];
                $get_user_id =  User::insertGetId($user);
                if (!empty($get_user_id)) {
                    $random = (string)mt_rand(10,1000000);
                    $token = (string)$get_user_id.$random;
                    $token_en = Crypt::encrypt($token);
                    $update_token = [
                        'remember_token' => $token_en,
                        'updated_at' => date('Y-m-d h:i:s')
                    ];
                    $update = User::where(['id'=>$get_user_id])->update($update_token);
                    if($update){
                        return response()->json([
                            'message'=>'Registered successfully',
                            'status'=>'1',
                            'user_id'=>(String)$get_user_id,
                            'token'=>$token_en,
                        ], $this->successStatus);
                    }
                }
                else{
                    return response()->json([
                        'message'=>'Signup Failed. Please try again later.',
                        'status'=>'1'
                    ], $this->successStatus);
                }
            }
        }
    }

    public function accountDetail(Request $request){
        try{
            $token_de = $request->header('token');
        } catch (DecryptException $e){
            return response()->json(['status' => '0', 'message' => $e]);
        }
        $existingUser = User::where(['remember_token'=>$token_de])->first();
        if($existingUser){
            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully', 
                'password_set' => $existingUser->password_set,
            ]);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getOwnProfile(Request $request){
        try{
            $token_de = $request->header('token');
        } catch (DecryptException $e){
            return response()->json(['status' => '0', 'message' => $e]);
        }
        $existingUser = User::where(['remember_token'=>$token_de])->first();
        if($existingUser){
            $langArray = '';
            $interestArray = '';
            // print_r($existingUser);die();
            if ($existingUser->login_type == '') {
                $image = asset('/user_images/' . $existingUser->image);
                // $image = 'nrmal';
            }else{
                $image = $existingUser->image;
                // $image = 'sl';
            }
            $checkAct = Activities::where('user_id',$existingUser->id)->first();
            if ($checkAct) {
                $lang = explode(",", $checkAct['languages']);
                $getLangs = Language::whereIn('id',$lang)->get();
                $langArray = [];
                foreach ($getLangs as $value) {
                    $listing['id'] = (string)$value['id'];
                    $listing['Interset'] = $value['language'];
                    $langArray[] = $listing;
                }
                $inter = explode(",", $checkAct['interests']);
                $getInter = Interest::whereIn('id',$inter)->get();
                $interestArray = [];
                foreach($getInter as $interest){
                    $intimage = url('/interest',$interest['image']);
                    $listing['id'] = (string)$interest['id'];
                    $listing['Interset'] = $interest['interset_name'];
                    $listing['image'] = $intimage;
                    $interestArray[] = $listing;
                }
            }
            $type = 'U';
            /*get followers list*/
            $getUserFollowers = $this->followers($type, $existingUser['id'])->toArray();
            $followerArr = array();
            foreach ($getUserFollowers as $follower) {
                $user1 = User::where('id',$follower['user_id'])->first();
                $uimage1 = asset('/user_images/' . $user1->image);
                $follow['user_id'] = $follower['user_id'];
                $follow['name'] = $user1['name'];
                $follow['image'] = $uimage1;
                $followerArr[] = $follow;
            }
            /*get followers list*/

            /*get followings list*/
            $getUserFollowings = $this->followings($type, $existingUser['id'])->toArray();
            $followingArr = array();
            foreach ($getUserFollowings as $following) {
                $user2 = User::where('id',$following['object_id'])->first();
                $uimage2 = asset('/user_images/' . $user2->image);
                $followin['user_id'] = $following['object_id'];
                $followin['name'] = $user2['name'];
                $followin['image'] = $uimage2;
                $followingArr[] = $followin;
            }
            /*get followers list*/

            /*Pages follow by current user*/
            $ptype = 'P';
            $getPageFollowings = $this->followings($ptype, $existingUser['id']);
            $threeArr = array();
            foreach ($getPageFollowings as $following) {
                $page = Pages::where('id',$following['object_id'])->first();
                $cover_photo = asset('/pages_images/' . $page->cover_photo);
                $background_photo = asset('/pages_images/' . $page->background_photo);
                $pages['page_id'] = $following['object_id'];
                $pages['page_title'] = $page['page_title'];
                $pages['cover_photo'] = $cover_photo;
                $pages['background_photo'] = $background_photo;
                $threeArr[] = $pages;
            }
            /*Pages follow by current user*/

            /*timeline posts shared by me*/
            $timelinePosts = Share::where('user_id',$existingUser->id)->select('post_id')->get()->toArray();
            $posts = Post::whereIn('id',$timelinePosts)
            ->orderBy('created_at','DESC')
            ->get();
            $postArr = [];
            foreach ($posts as $post ) {
                /*likes*/
                $likeArr = [];
                $getLikes = LikeComment::where('post_id',$post['id'])
                ->where('isliked','!=','')
                ->get();
                foreach ($getLikes as $like) {
                    $user = User::where('id',$like['user_id'])->first();
                    $usermedia = asset('/user_images/' . $user['image']);
                    $list['user_id'] = (string)$like['user_id'];
                    $list['user_name'] = $user['name'];
                    $list['profile_pic'] = $usermedia;
                    $list['isliked'] = $like['isliked'];
                    $likeArr[] = $list;
                }
                /*likes*/
                /*comments*/
                $commentArr = [];
                $getComments = LikeComment::where('post_id',$post['id'])
                ->where('comment','!=','')
                ->get();
                foreach ($getComments as $comment) {
                    $user = User::where('id',$comment['user_id'])->first();
                    $usermedia = asset('/user_images/' . $user['image']);
                    $clist['user_id'] = (string)$comment['user_id'];
                    $clist['user_name'] = $user['name'];
                    $clist['profile_pic'] = $usermedia;
                    $clist['comment'] = $comment['comment'];
                    $commentArr[] = $clist;
                }
                /*comments*/
                /*media*/
                $media = explode(",", $post['media']);
                $mediaArray = [];
                foreach ($media as $med) {
                    $media = asset('/post_media/' . $med);
                    $medi['media'] = $media;
                    $mediaArray[] = $medi;
                }
                /*media*/
                /*page*/
                $postData = Post::where('id',$post['id'])->first();
                $pageData = Pages::where('id',$postData['page_id'])->first();
                $cover_photo = asset('/pages_images/' . $pageData['cover_photo']);
                $background_photo = asset('/pages_images/' . $pageData['background_photo']);
                /*page*/
                $getthumbLikes = $this->thumblikesCount($post['id']);
                $getHeartLikes = $this->heartlikesCount($post['id']);
                $getSmileLikes = $this->smilelikesCount($post['id']);
                $getFlowerLikes = $this->flowerlikesCount($post['id']);
                $lists['post_id'] = (string)$post['id'];
                $lists['page_id'] = (string)$pageData['id'];
                $lists['page_title'] = $pageData['page_title'];
                $lists['cover_photo'] = $cover_photo;
                $lists['background_photo'] = $background_photo;
                $lists['post_desc'] = $post['post_desc'];
                $lists['media'] = $mediaArray;
                $lists['likes_count'] = count($likeArr);
                $lists['thumbs_count'] = count($getthumbLikes);
                $lists['heart_count'] = count($getHeartLikes);
                $lists['smile_count'] = count($getSmileLikes);
                $lists['flower_count'] = count($getFlowerLikes);
                $lists['likes'] = $likeArr;
                $lists['comments_count'] = count($commentArr);
                $lists['comments'] = $commentArr;
                $postArr[] = $lists;
            }
            /*timeline posts shared by me*/
            /*tagged posts*/
            $tagged_post = Post::whereRaw('FIND_IN_SET('.$existingUser->id.',tagged_users)')
            ->type()->status()
            // ->orWhere('type','B')
            ->orderBy('created_at','DESC')
            ->get();
            $taggedpostArr = [];
            foreach ($tagged_post as $tag_post ) {
                $getthumbLikes = $this->thumblikesCount($tag_post['id']);
                $getHeartLikes = $this->heartlikesCount($tag_post['id']);
                $getSmileLikes = $this->smilelikesCount($tag_post['id']);
                $getFlowerLikes = $this->flowerlikesCount($tag_post['id']);
                $taglikeArr = [];
                $gettagLikes = LikeComment::where('post_id',$tag_post['id'])
                ->where('isliked','!=','')
                ->get();
                foreach ($gettagLikes as $taglike) {
                    $taguser = User::where('id',$taglike['user_id'])->first();
                    $usermedia = asset('/user_images/' . $taguser['image']);
                    $taglist['user_id'] = (string)$taglike['user_id'];
                    $taglist['user_name'] = $taguser['name'];
                    $taglist['profile_pic'] = $usermedia;
                    $taglist['isliked'] = $taglike['isliked'];
                    $taglikeArr[] = $taglist;
                }
                $tagcommentArr = [];
                $taggetComments = LikeComment::where('post_id',$tag_post['id'])
                ->where('comment','!=','')
                ->get();
                foreach ($taggetComments as $tagcomment) {
                    $user = User::where('id',$tagcomment['user_id'])->first();
                    $usermedia = asset('/user_images/' . $user['image']);
                    $ctaglist['user_id'] = (string)$tagcomment['user_id'];
                    $ctaglist['user_name'] = $user['name'];
                    $ctaglist['profile_pic'] = $usermedia;
                    $ctaglist['comment'] = $tagcomment['comment'];
                    $tagcommentArr[] = $ctaglist;
                }
                $tagmedia = explode(",", $tag_post['media']);
                $tagmediaArray = [];
                foreach ($tagmedia as $tagmed) {
                    $tmedia = asset('/post_media/' . $tagmed);
                    $tmedi['media'] = $tmedia;
                    $tagmediaArray[] = $tmedi;
                }
                $tlists['post_id'] = (string)$tag_post['id'];
                $tlists['post_desc'] = $tag_post['post_desc'];
                $tlists['media'] = $tagmediaArray;
                $tlists['likes_count'] = count($taglikeArr);
                $tlists['likes'] = $taglikeArr;
                $tlists['comments_count'] = count($tagcommentArr);
                $tlists['comments'] = $tagcommentArr;
                $tlists['thumbs_count'] = count($getthumbLikes);
                $tlists['heart_count'] = count($getHeartLikes);
                $tlists['smile_count'] = count($getSmileLikes);
                $tlists['flower_count'] = count($getFlowerLikes);
                $taggedpostArr[] = $tlists;
            }

            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully',
                'username' => $existingUser->username,
                'name' => $existingUser->name,
                'email' => $existingUser->email,
                'phone_no' => $existingUser->phone_no,
                'DOB' => $existingUser->DOB,
                'gender' => $existingUser->gender,
                'followers_count' => count($followerArr),
                'following_count' => count($followingArr),
                'pages_count' => count($threeArr),
                'followers' => $followerArr,
                'following' => $followingArr,
                'pages' => $threeArr,
                'bio' => '',
                'languages' => $langArray?$langArray:'',
                'interests' => $interestArray?$interestArray:'',
                'image' => $image,
                'timeline_posts' => $postArr,
                'tagged_posts' => $taggedpostArr,
            ]);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function editProfile(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where('remember_token', $token)->first();
        if($userExist){
            if($request->hasFile('image')){
                $image= time().$request->image->getClientOriginalName();
                $request->image->move(public_path('/user_images') . '/', $image);
            }else{
                $image = $userExist['image'];
            }
            $email = '';
            if (!empty($saveArray['email'])) {
                $checkEmail = User::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json(['status'=>'0','message'=>'Email already exists'
                    ], $this->successStatus);
                }else {
                    $email = $saveArray['email'];
                }
            }
            $phone_no = '';
            if (!empty($saveArray['phone_no'])) {
                $checkPhone = User::where('phone_no',$saveArray['phone_no'])->first();
                if ($checkPhone) {
                    return response()->json(['status'=>'1','message'=>'Phone already exists'
                    ], $this->successStatus);
                }else {
                    $phone_no = $saveArray['phone_no'];
                }
            }
            $checkAct = Activities::where('user_id',$userExist->id)->first();
            $languages = array($saveArray['user_language']);
            $a = str_replace(array( '[', ']' ), '', $languages);
            $lang = implode(",", $a);
            
            $activity = array($saveArray['interests']);
            $b = str_replace(array( '[', ']' ), '', $activity);
            $act = implode(",", $b);

            if (!empty($saveArray['user_language']) || !empty($saveArray['interests'])) {
                $updateAct = [
                    'languages'=>$lang?$lang:$checkAct['languages'],
                    'interests'=>$act?$act:$checkAct['interests'],
                ];
                $update = Activities::where('user_id',$userExist->id)->update($updateAct);
            }
            $updateProfile = [
                'username'=>$saveArray['username']?$saveArray['username']:$userExist['username'],
                'name'=>$saveArray['name']?$saveArray['name']:$userExist['name'],
                'gender'=>$saveArray['gender']?$saveArray['gender']:$userExist['gender'],
                'dob'=>$saveArray['dob']?$saveArray['dob']:$userExist['dob'],
                'email'=>$email?$email:$userExist['email'],
                'phone_no'=>$phone_no?$phone_no:$userExist['phone_no'],
                'image'=>$image,
                'updated_at'=>date('Y-m-d h:i:s')
            ];
            $update = User::where(['remember_token'=>$token])->update($updateProfile);
            if($update){
                $userDetails = User::where(['remember_token'=>$token])->first();
                $image = asset('/user_images/' . $userDetails->image);
                $userDetails['image'] = $image;
                $updatedAct = Activities::where('user_id',$userDetails->id)->first();
                $langArray = '';
                $interestArray = '';
                if ($updatedAct) {
                    $lang = explode(",", $updatedAct['languages']);
                    $getLangs = Language::whereIn('id',$lang)->get();
                    $langArray = [];
                    foreach ($getLangs as $value) {
                        $listing['id'] = (string)$value['id'];
                        $listing['Interset'] = $value['language'];
                        $langArray[] = $listing;
                    }
                    $inter = explode(",", $updatedAct['interests']);
                    $getInter = Interest::whereIn('id',$inter)->get();
                    $interestArray = [];
                    foreach($getInter as $interest){
                        $intimage = url('/interest',$interest['image']);
                        $list['id'] = (string)$interest['id'];
                        $list['interset'] = $interest['interset_name'];
                        $list['image'] = $intimage;
                        $interestArray[] = $list;
                    }
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'User Updated Successfully',
                    'user_details'=>$userDetails,
                    'languages' => $langArray?$langArray:'',
                    'interests' => $interestArray?$interestArray:'',
                ], $this->successStatus);
            }
            else{
                return response()->json(['message'=>'User does not Update','status'=>'0'], $this->badrequest);
            }
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function forgotPassword(Request $request){
        $saveArray = $request->all();
        if (User::where('email', '=', $saveArray['email'])->count() > 0) {
            $new_password= $this->randomPassword();
            $new_password_en = Hash::make($new_password);
            $update_password = [
                'password' => $new_password_en,
                'updated_at' => date('Y-m-d h:i:s')
            ];
            $update = User::where(['email'=>$saveArray['email']])->update($update_password);
            if($update){
                $name = $saveArray['email'];
                $msg = [
                    "name" => $name,
                    "new_password" => $new_password
                ];
                $email = $saveArray['email'];
                Mail::send('mail', $msg, function($message) use ($email) {
                    $message->to($email);
                    $message->subject('SocialApp');
                });
                return response()->json(['status' =>'1' ,'message'=>'New Password has been mailed to you. Please check your email'], $this->successStatus);
            }
            // $response = $this->broker()->sendResetLink(
            //     $request->only('email')
            // );
            // if($response == 'passwords.sent'){
            //     $response = 'We have e-mailed your password reset link!';
            //     return response()->json(['status' =>'1' ,'message'=>'success', 'data'=> $response], $this->successStatus);
            // } else{
            //     $response = 'Your password reset email not sent!';
            //     return response()->json(['status' =>'0' ,'message'=>'error', 'data'=> $response], $this->successStatus);
            // }
        } else{
            return response()->json(['status' =>'error' ,'message'=>'Unauthorised'], 401);
        }
    }

    public function changePassword(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
          ]);
          if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
          }
          elseif ($request->old_password == $request->new_password) {
            return response()->json(['message'=>'New password is same as old password','status'=>'0'], $this->successStatus);
          }
          else{
              if(Hash::check($request->old_password, $existingUser->password)){
                $password_en =  Hash::make($request->new_password);
                $update = User::where(['id'=>$existingUser->id])->update(['password' => $password_en]);
                // echo $update;
                if($update){
                  return response()->json([
                        'status'=>'1',
                        'message'=>'Password changed Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on changing password'
                      ], $this->successStatus);
                }
              }
              else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'Old password is incorrect'
                ], $this->successStatus);
            }
        }
    }

    public function addActivity(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'user_language' => 'required',
            'interests' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $checkAct = Activities::where('user_id',$existingUser->id)->first();
                $languages = array($saveArray['user_language']);
                $a = str_replace(array( '[', ']' ), '', $languages);
                $lang = implode(",", $a);
                $activity = array($saveArray['interests']);
                $b = str_replace(array( '[', ']' ), '', $activity);
                $act = implode(",", $b);
                if ($checkAct) {
                    # update...
                    $update = Activities::where(['user_id'=>$existingUser->id])->update([
                        'languages' => $lang,
                        'interests' => $act,
                    ]);
                    if ($update) {
                        return response()->json(['status'=>'1','message'=>'Activities updated'], $this->successStatus);
                    }
                    else{
                        return response()->json(['message'=>'Something went wrong','status'=>'0'
                    ], $this->badrequest);
                    }
                }else{
                    # insert
                    $addactivity = [
                        'user_id'=>$existingUser['id'],
                        'languages' => $lang,
                        'interests' => $act,
                        'created_at' => date('Y-m-d h:i:s')
                    ];
                    $insert =  Activities::insertGetId($addactivity);
                    if ($insert) {
                        return response()->json(['status'=>'1','message'=>'Activities Created'], $this->successStatus);
                    }
                    else{
                        return response()->json(['message'=>'Something went wrong','status'=>'0'
                    ], $this->badrequest);
                    }
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                ], $this->badrequest);
            }
        }
    }

    public function follow(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'object_id' => 'required', #id of a page user and post and anything else
            'object_type' => 'required',
            'act' => 'required' #FOLLOW', 'UNFOLLOW', 'VISIT', 'LIKE', 'DISLIKE
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                if ($request->object_type == 'PAGE') {
                    if ($request->act == 'FOLLOW') {
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'PAGE','status' => 'followed'];
                        $requested = Action::where($matchThese)->first();
                        if($requested){
                            return response()->json(['status'=>'0','message'=>'You already followed this page'], $this->successStatus);
                        }else {
                            $follow= new Action;
                            $follow->user_id = $existingUser['id']; 
                            $follow->object_id = $request->object_id;
                            $follow->act = $request->act;
                            $follow->object_type = $request->object_type;
                            $follow->status = 'followed'; 
                            $follow->save();
                            return response()->json(['status' => '1','message'=>'Followed successfully'], $this->successStatus);
                        }
                    }
                    elseif ($request->act == 'UNFOLLOW') {
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'PAGE','status' => 'followed'];
                        $requested = Action::where($matchThese)->first();
                        if($requested){
                            $update = Action::where('user_id',$existingUser->id)
                            ->where('object_id',$request->object_id)
                            ->where('object_type',$request->object_type)
                            ->delete();
                            return response()->json(['status' => '1','message'=>'unfollowed successfully'], $this->successStatus);
                        }else{
                            return response()->json(['status'=>'0','message'=>'no data'], $this->successStatus);
                        }
                    }
                }elseif ($request->object_type == 'USER') {
                    // dd("USER");
                    if ($request->act == 'FOLLOW') {
                        // dd("user FOLLOW");
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'USER','status' => 'followed'];
                        $requested = Action::where($matchThese)->first();
                        if($requested){
                            return response()->json(['status'=>'0','message'=>'You already followed this account'], $this->successStatus);
                        }else {
                            $checkBlocking = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'USER','status' => 'blocked'];
                            $blocked = Action::where($checkBlocking)->first();
                            if ($blocked) {
                                return response()->json(['status'=>'0','message'=>'You blocked this account please unblock first then follow again'], $this->successStatus);
                            }else{
                                $follow= new Action;
                                $follow->user_id = $existingUser['id']; 
                                $follow->object_id = $request['object_id'];
                                $follow->act = $request->act;
                                $follow->object_type = $request->object_type;
                                $follow->status = 'followed'; 
                                $follow->save();
                                /*fire follow notification*/
                                $deviceIds = User::where('id',$request['object_id'])
                                ->select('device_id','device_type','username')
                                ->first();
                                $deviceArr = $deviceIds['device_id'];
                                $message = ''.$existingUser['username'].' started following you ';
                                if ($deviceIds['device_type'] == 'ANDROID') {
                                    $followNoti = $this->android_send_notification($deviceArr,$message,$request['object_id']);
                                }else{
                                    $followNoti = $this->iphone_send_notifications($deviceArr,$message,$request['object_id']);
                                }

                                $notificationArr = array(
                                    'user_id'=> $existingUser['id'],
                                    'object_id'=> $request['object_id'],
                                    'message'=> ''.$existingUser['username'].' started following you ',
                                    'act'=> "USER",
                                    'read_status'=> "0",
                                );

                                $saveNoti = $this->storeNotification($notificationArr);
                                /*fire follow notification*/
                                return response()->json(['status' => '1','message'=>'Followed successfully'], $this->successStatus);
                            }
                        }
                    }
                    elseif ($request->act == 'UNFOLLOW') {
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'USER','status' => 'followed'];
                        $requested = Action::where($matchThese)->first();
                        if($requested){
                            $updateAct = ['status'=>'removed'];
                            $update = Action::where('user_id',$existingUser->id)
                            ->where('object_id',$request->object_id)
                            ->where('object_type',$request->object_type)
                            ->update($updateAct);
                            return response()->json(['status' => '1','message'=>'unfollowed successfully'], $this->successStatus);
                        }else{
                            return response()->json(['status'=>'0','message'=>'no data'], $this->successStatus);
                        }
                    }
                    elseif ($request->act == 'BLOCK') {
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'USER'];
                        $requested = Action::where($matchThese)->first();
                        if($requested){
                            $updateAct = ['act'=>'BLOCK','status'=>'blocked'];
                            $blockUser = Action::where('user_id',$existingUser->id)
                            ->where('object_id',$request->object_id)
                            ->where('object_type',$request->object_type)
                            ->orWhere('object_id',$existingUser->id)
                            ->where('user_id',$request->object_id)
                            ->where('object_type',$request->object_type)
                            ->update($updateAct);
                            return response()->json(['status' => '1','message'=>'blocked successfully'], $this->successStatus);
                        }else{
                            $follow= new Action;
                            $follow->user_id = $existingUser['id']; 
                            $follow->object_id = $request->object_id;
                            $follow->act = $request->act;
                            $follow->object_type = $request->object_type;
                            $follow->status = 'blocked'; 
                            $follow->save();
                            return response()->json(['status' => '1','message'=>'Blocked successfully'], $this->successStatus);
                        }
                    }
                    elseif ($request->act == 'UNBLOCK') {
                        $matchThese = ['user_id' => $existingUser->id, 'object_id' => $request->object_id, 'object_type' => 'USER','status' => 'blocked'];
                        $requested = Action::where($matchThese)->first();
                        // print_r($requested);die();
                        if($requested){
                            $update = Action::where('user_id',$existingUser->id)
                            ->where('object_id',$request->object_id)
                            ->where('object_type',$request->object_type)
                            ->delete();
                            return response()->json(['status' => '1','message'=>'unblocked successfully'], $this->successStatus);
                        }else{
                            return response()->json(['status'=>'0','message'=>'no data'], $this->successStatus);
                        }
                    }
                }
            }else{
                return response()->json(['message'=>'User not found','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function getNotifications(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $userExist = User::where('id', $request->user_id)->first();
            if($userExist){
                $catArr = [];
                $category = Notification::where('object_id',$request->user_id)->get();

                $larr = json_decode($category,true);
                // print_r($category);die();
                foreach($larr AS $cat){
                    $user = User::where('id', $cat['user_id'])->first();
                    $userImage = asset('/user_images/' . $user['image']);
                    if ($user['login_type'] == Null) {
                        $usermedia = $userImage;
                    }else{
                        $usermedia = $user['image'];
                    }
                    $list['notification_id'] = $cat['id'];
                    $list['user_id'] = $cat['user_id'];
                    $list['user_image'] = $usermedia;
                    $list['object_id'] = $cat['object_id'];
                    $list['act'] = $cat['act'];
                    $list['message'] = $cat['message'];
                    $list['date'] = $cat['created_at'];
                    $catArr[] = $list;
                }
                return response()->json([
                    'status'=>'true',
                    'message'=>'Notifications fetched successfully',
                    'notifications'=>$catArr
                ], $this->successStatus);
            }else{
                return response()->json(['status'=>'false','message'=>'User not found'], $this->badrequest);
            }
        }
    }

    public function blockedLists(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token',$token)->first();
        if ($existingUser) {
            $blockedUsers = $this->blockedUsers($existingUser['id']);
            $blockedArr = array();
            foreach ($blockedUsers as $blocked) {
                $userDetail = User::where('id',$blocked['object_id'])->first();
                $userimage = asset('/user_images/' . $userDetail->image);
                $block['user_id'] = $blocked['object_id'];
                $block['name'] = $userDetail['name'];
                $block['image'] = $userimage;
                $blockedArr[] = $block;
            }
            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully',
                'blockedArray' => $blockedArr,
                'blocked_count' => count($blockedArr)
            ]);
        }else{
            return response()->json(['message'=>'User not exists','status'=>'0'
                ], $this->badrequest);
        }
    }

    public function suggessions(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userActs = Activities::where('user_id',$request->user_id)
            ->orwhere('languages','!=','')
            ->orwhere('interests','!=','')
            ->first();
            if ($userActs) {
                $languArr = $userActs['languages'];
                $larray = explode(",", $languArr);
                foreach ($larray as $key) {
                    $langu = Pages::where('languages', 'like', '%'.$key.'%')->get();
                    $laArray = [];
                    foreach ($langu as $pag ) {
                        $laArray[] = $pag;
                    }
                }
                $interArr = $userActs['interests'];
                $iarray = explode(",", $interArr);
                foreach ($iarray as $key1) {
                    $inter = Pages::where('interests', 'like', '%'.$key1.'%')->get();
                    $inArray = [];
                    foreach ($inter as $inte ) {
                        $inArray[] = $inte;
                    }
                }
                $main = array_unique (array_merge ($laArray, $inArray));
                // print_r(json_encode($main));die();
                $mainArray= [];
                foreach ($main as $ma ) {
                    $image = asset('/pages_images/' . $ma['cover_photo']);
                    $list['id'] = (string)$ma['id'];
                    $list['page_title'] = $ma['page_title'];
                    $list['cover_photo'] = $image;
                    $mainArray[] = $list;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'suggessions fetchedd successfully',
                    'pages'=>$mainArray
                ], $this->successStatus);
            }else {
                return response()->json(['message'=>'no suggessions found','status'=>'0'], $this->badrequest);
            }
        }
    }
    /*1=active,2=deleted,3=archieved,4=banned,5=deleted*/
    public function deleteAccount(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token',$token)->first();
        if ($existingUser) {
            $getPages = Pages::where('user_id',$existingUser['id'])
            ->select('id')
            ->get()
            ->toArray();
            // print_r($getPages);die();
            $updateStatus = ['status'=>'2'];
            $update = Post::whereIn('page_id',$getPages)->update($updateStatus);
            // $updateStatus = Post::whereIn('page_id',$getPages);
            $user = User::where('remember_token','=',$token)->update(['deleted_at' => date('Y-m-d h:i:s')]);
            if ($user) {
                return response()->json(['message'=>'Account deleted permanentaly','status'=>'1'
                ], $this->successStatus);
            }else{
                return response()->json(['message'=>'Something went wrong','status'=>'0'
                ], $this->badrequest);
            }
        }else{
            return response()->json(['message'=>'User not exists','status'=>'0'
                ], $this->badrequest);
        }
    }

    public function search(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'search' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token',$token)->first();
            if ($existingUser) {
                $blockedMe = Action::where('user_id',$existingUser['id'])
                ->where('object_type','USER')
                ->where('status','blocked')
                ->select('object_id')
                ->get()
                ->toArray();
                $in = $blockedMe;
                $usersSearch = User::where('type', '!=', 'A')
                ->where('id', '!=', $existingUser['id'])
                ->whereNotIn('id',$in)
                ->where('name', 'LIKE', '%' . $request->search . '%')
                // ->orWhere('email', 'LIKE', '%' . $request->search . '%')
                ->orWhere('username', 'LIKE', '%' . $request->search . '%')
                ->select('id','username','name','image','type')
                ->get()
                ->toArray();
                $pagesSearch = Pages::where('page_title', 'LIKE', '%' . $request->search . '%')
                ->select('id', 'page_title', 'cover_photo', 'type')
                ->get()->toArray();
                $users = array_merge ($usersSearch, $pagesSearch);
                $searchArr = [];
                foreach($users AS $user){
                    if ($user['type'] == 'U') {
                        $name = $user['name'];
                        $username = $user['username'];
                        $type = 'U';
                        $image = asset('/user_images/' . $user['image']);
                        $usermatchThese = ['user_id' => $existingUser['id'],'object_id' => $user['id'],'object_type' => 'USER','status' => 'followed'];
                        $ufollowStatus = Action::where($usermatchThese)->first();
                        if ($ufollowStatus) {
                            $isFollowed = "Yes";
                        }else{
                            $isFollowed = "No";
                        }
                    }
                    if($user['type'] == 'P'){
                        $type = 'P';
                        $name = $user['page_title'];
                        $username = '';
                        $image = asset('/pages_images/' . $user['cover_photo']);
                        $pagematchThese = ['user_id' => $existingUser['id'],'object_id' => $user['id'],'object_type' => 'PAGE','status' => 'followed'];
                        $pfollowStatus = Action::where($pagematchThese)->first();
                        if ($pfollowStatus) {
                            $isFollowed = "Yes";
                        }else{
                            $isFollowed = "No";
                        }
                    }
                    $slist['id'] = (String)$user['id'];
                    $slist['name'] = $name;
                    $slist['username'] = $username;
                    $slist['image'] = $image;
                    $slist['type'] = $type;
                    $slist['isFollowed'] = $isFollowed;
                    $searchArr[] = $slist;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'searchArray'=>$searchArr
                ], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                ], $this->badrequest);
            }
        }
    }

    public function viewUser(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $tokenUser = User::where(['remember_token'=>$token])->first();
            $existingUser = User::where(['id'=>$saveArray['user_id']])->first();
            if($existingUser){
                // $existingUser = User::where(['id'=>$saveArray['user_id']])->first();
                $langArray = '';
                $interestArray = '';
                $image = asset('/user_images/' . $existingUser->image);
                $usermatchThese = ['user_id' => $tokenUser['id'],'object_id' => $saveArray['user_id'],'object_type' => 'USER','status' => 'followed'];
                $ufollowStatus = Action::where($usermatchThese)->first();
                if ($ufollowStatus) {
                    $isFollowed = "Yes";
                }else{
                    $isFollowed = "No";
                }
                $checkAct = Activities::where('user_id',$existingUser->id)->first();
                if ($checkAct) {
                    $lang = explode(",", $checkAct['languages']);
                    $getLangs = Language::whereIn('id',$lang)->get();
                    $langArray = [];
                    foreach ($getLangs as $value) {
                        $listing['id'] = (string)$value['id'];
                        $listing['Interset'] = $value['language'];
                        $langArray[] = $listing;
                    }
                    $inter = explode(",", $checkAct['interests']);
                    $getInter = Interest::whereIn('id',$inter)->get();
                    $interestArray = [];
                    foreach($getInter as $interest){
                        $intimage = url('/interest',$interest['image']);
                        $listing['id'] = (string)$interest['id'];
                        $listing['Interset'] = $interest['interset_name'];
                        $listing['image'] = $intimage;
                        // $listing['created_at'] = $interest['created_at'];
                        $interestArray[] = $listing;
                    }
                }
                $type = 'U';
                /*get followers list*/
                $getUserFollowers = $this->followers($type, $existingUser['id'])->toArray();
                $followerArr = array();
                foreach ($getUserFollowers as $follower) {
                    $user1 = User::where('id',$follower['user_id'])->first();
                    $uimage1 = asset('/user_images/' . $user1->image);
                    $follow['user_id'] = $follower['user_id'];
                    $follow['name'] = $user1['name'];
                    $follow['image'] = $uimage1;
                    $followerArr[] = $follow;
                }
                /*get followers list*/

                /*get followings list*/
                $getUserFollowings = $this->followings($type, $existingUser['id'])->toArray();
                $followingArr = array();
                foreach ($getUserFollowings as $following) {
                    $user2 = User::where('id',$following['object_id'])->first();
                    $uimage2 = asset('/user_images/' . $user2->image);
                    $followin['user_id'] = $following['object_id'];
                    $followin['name'] = $user2['name'];
                    $followin['image'] = $uimage2;
                    $followingArr[] = $followin;
                }
                /*get followings list*/

                /*Pages follow by current user*/
                $ptype = 'P';
                $getPageFollowings = $this->followings($ptype, $existingUser['id']);
                $threeArr = array();
                foreach ($getPageFollowings as $following) {
                    $page = Pages::where('id',$following['object_id'])->first();
                    $cover_photo = asset('/pages_images/' . $page->cover_photo);
                    $background_photo = asset('/pages_images/' . $page->background_photo);
                    $pages['page_id'] = $following['object_id'];
                    $pages['page_title'] = $page['page_title'];
                    $pages['cover_photo'] = $cover_photo;
                    $pages['background_photo'] = $background_photo;
                    $threeArr[] = $pages;
                }

                /*timeline posts shared by me*/
                $timelinePosts = Share::where('user_id',$existingUser->id)->select('post_id')->get()->toArray();
                $posts = Post::whereIn('id',$timelinePosts)
                ->orderBy('created_at','DESC')
                ->get();
                $postArr = [];
                foreach ($posts as $post ) {
                    $getthumbLikes = $this->thumblikesCount($post['id']);
                    $getHeartLikes = $this->heartlikesCount($post['id']);
                    $getSmileLikes = $this->smilelikesCount($post['id']);
                    $getFlowerLikes = $this->flowerlikesCount($post['id']);
                    $likeArr = [];
                    $getLikes = LikeComment::where('post_id',$post['id'])
                    ->where('isliked','!=','')
                    ->get();
                    foreach ($getLikes as $like) {
                        $user = User::where('id',$like['user_id'])->first();
                        $usermedia = asset('/user_images/' . $user['image']);
                        $list['user_id'] = (string)$like['user_id'];
                        $list['user_name'] = $user['name'];
                        $list['profile_pic'] = $usermedia;
                        $list['isliked'] = $like['isliked'];
                        $likeArr[] = $list;
                    }
                    $commentArr = [];
                    $getComments = LikeComment::where('post_id',$post['id'])
                    ->where('comment','!=','')
                    ->get();
                    foreach ($getComments as $comment) {
                        $user = User::where('id',$comment['user_id'])->first();
                        $usermedia = asset('/user_images/' . $user['image']);
                        $clist['user_id'] = (string)$comment['user_id'];
                        $clist['user_name'] = $user['name'];
                        $clist['profile_pic'] = $usermedia;
                        $clist['comment'] = $comment['comment'];
                        $commentArr[] = $clist;
                    }
                    $media = explode(",", $post['media']);
                    $mediaArray = [];
                    foreach ($media as $med) {
                        $media = asset('/post_media/' . $med);
                        $medi['media'] = $media;
                        $mediaArray[] = $medi;
                    }
                    $lists['post_id'] = (string)$post['id'];
                    $lists['post_desc'] = $post['post_desc'];
                    $lists['media'] = $mediaArray;
                    $lists['likes_count'] = count($likeArr);
                    $lists['likes'] = $likeArr;
                    $lists['comments_count'] = count($commentArr);
                    $lists['comments'] = $commentArr;
                    $postArr[] = $lists;
                }
                /*timeline posts shared by me*/

                /*tagged posts*/
                $tagged_post = Post::whereRaw('FIND_IN_SET('.$existingUser->id.',tagged_users)')
                ->type()->status()
                // ->orWhere('type','B')
                ->orderBy('created_at','DESC')
                ->get();
                $taggedpostArr = [];
                foreach ($tagged_post as $tag_post ) {
                    $taglikeArr = [];
                    $gettagLikes = LikeComment::where('post_id',$tag_post['id'])
                    ->where('isliked','!=','')
                    ->get();
                    foreach ($gettagLikes as $taglike) {
                        $taguser = User::where('id',$taglike['user_id'])->first();
                        $usermedia = asset('/user_images/' . $taguser['image']);
                        $taglist['user_id'] = (string)$taglike['user_id'];
                        $taglist['user_name'] = $taguser['name'];
                        $taglist['profile_pic'] = $usermedia;
                        $taglist['isliked'] = $taglike['isliked'];
                        $taglikeArr[] = $taglist;
                    }
                    $tagcommentArr = [];
                    $taggetComments = LikeComment::where('post_id',$tag_post['id'])
                    ->where('comment','!=','')
                    ->get();
                    foreach ($taggetComments as $tagcomment) {
                        $user = User::where('id',$tagcomment['user_id'])->first();
                        $usermedia = asset('/user_images/' . $user['image']);
                        $ctaglist['user_id'] = (string)$tagcomment['user_id'];
                        $ctaglist['user_name'] = $user['name'];
                        $ctaglist['profile_pic'] = $usermedia;
                        $ctaglist['comment'] = $tagcomment['comment'];
                        $tagcommentArr[] = $ctaglist;
                    }
                    $tagmedia = explode(",", $tag_post['media']);
                    $tagmediaArray = [];
                    foreach ($tagmedia as $tagmed) {
                        $tmedia = asset('/post_media/' . $tagmed);
                        $tmedi['media'] = $tmedia;
                        $tagmediaArray[] = $tmedi;
                    }
                    $tlists['post_id'] = (string)$tag_post['id'];
                    $tlists['post_desc'] = $tag_post['post_desc'];
                    $tlists['media'] = $tagmediaArray;
                    $tlists['likes_count'] = count($taglikeArr);
                    $tlists['thumbs_count'] = count($getthumbLikes);
                    $tlists['heart_count'] = count($getHeartLikes);
                    $tlists['smile_count'] = count($getSmileLikes);
                    $tlists['flower_count'] = count($getFlowerLikes);
                    $tlists['likes'] = $taglikeArr;
                    $tlists['comments_count'] = count($tagcommentArr);
                    $tlists['comments'] = $tagcommentArr;
                    $taggedpostArr[] = $tlists;
                }
                /*Pages follow by current user*/
                return response()->json([
                    'status'=>'1',
                    'message'=>'User fetched successfully',
                    'id' => (String)$existingUser->id,
                    'username' => $existingUser->username,
                    'name' => $existingUser->name,
                    'email' => $existingUser->email,
                    'phone_no' => $existingUser->phone_no,
                    'DOB' => $existingUser->DOB,
                    'gender' => $existingUser->gender,
                    'followers_count' => count($followerArr),
                    'following_count' => count($followingArr),
                    'pages_count' => count($threeArr),
                    'followers' => $followerArr,
                    'following' => $followingArr,
                    'pages' => $threeArr,
                    'posts' => '',
                    'bio' => '',
                    'languages' => $langArray?$langArray:'',
                    'interests' => $interestArray?$interestArray:'',
                    'image' => $image,
                    'isFollowed' => $isFollowed,
                    'timeline_posts' => $postArr,
                    'tagged_posts' => $taggedpostArr,
                ]);
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }
    }

    public function getBuddies(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where(['remember_token'=>$token])->first();
        if($existingUser){
            $type = 'U';

            /*get followers list*/
            $getUserFollowers = $this->followers($type, $existingUser['id'])->toArray();
            $followerArr = array();
            foreach ($getUserFollowers as $follower) {
                $user1 = User::where('id',$follower['user_id'])->first();
                $uimage1 = asset('/user_images/' . $user1->image);
                $follow['user_id'] = $follower['user_id'];
                $follow['name'] = $user1['name'];
                $follow['username'] = $user1['username']?$user1['username']:'';
                $follow['image'] = $uimage1;
                $followerArr[] = $follow;
            }
            /*get followers list*/

            /*get followings list*/
            $getUserFollowings = $this->followings($type, $existingUser['id'])->toArray();
            $followingArr = array();
            foreach ($getUserFollowings as $following) {
                $user2 = User::where('id',$following['object_id'])->first();
                $uimage2 = asset('/user_images/' . $user2->image);
                $followin['user_id'] = $following['object_id'];
                $followin['name'] = $user2['name'];
                $followin['username'] = $user2['username']?$user2['username']:'';
                $followin['image'] = $uimage2;
                $followingArr[] = $followin;
            }
            // $result = array_unique (array_merge ($followerArr, $followingArr));
            $result = array_merge($followerArr, $followingArr);
            return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'buddiesArray'=>$result
                ], $this->successStatus);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function index(Request $request){
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request){
        $input = $request->all();

        $user = $this->userRepository->create($input);

        return $this->sendResponse($user->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id){
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request){
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id){
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }
}
