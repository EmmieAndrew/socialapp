<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInterestAPIRequest;
use App\Http\Requests\API\UpdateInterestAPIRequest;
use App\Models\Interest;
use App\Repositories\InterestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class InterestController
 * @package App\Http\Controllers\API
 */

class InterestAPIController extends AppBaseController
{
    /** @var  InterestRepository */
    private $interestRepository;

    public function __construct(InterestRepository $interestRepo)
    {
        $this->interestRepository = $interestRepo;
    }

    /**
     * Display a listing of the Interest.
     * GET|HEAD /interests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $interests = $this->interestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($interests->toArray(), 'Interests retrieved successfully');
    }

    /**
     * Store a newly created Interest in storage.
     * POST /interests
     *
     * @param CreateInterestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInterestAPIRequest $request)
    {
        $input = $request->all();

        $interest = $this->interestRepository->create($input);

        return $this->sendResponse($interest->toArray(), 'Interest saved successfully');
    }

    /**
     * Display the specified Interest.
     * GET|HEAD /interests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Interest $interest */
        $interest = $this->interestRepository->find($id);

        if (empty($interest)) {
            return $this->sendError('Interest not found');
        }

        return $this->sendResponse($interest->toArray(), 'Interest retrieved successfully');
    }

    /**
     * Update the specified Interest in storage.
     * PUT/PATCH /interests/{id}
     *
     * @param int $id
     * @param UpdateInterestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInterestAPIRequest $request)
    {
        $input = $request->all();

        /** @var Interest $interest */
        $interest = $this->interestRepository->find($id);

        if (empty($interest)) {
            return $this->sendError('Interest not found');
        }

        $interest = $this->interestRepository->update($input, $id);

        return $this->sendResponse($interest->toArray(), 'Interest updated successfully');
    }

    /**
     * Remove the specified Interest from storage.
     * DELETE /interests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Interest $interest */
        $interest = $this->interestRepository->find($id);

        if (empty($interest)) {
            return $this->sendError('Interest not found');
        }

        $interest->delete();

        return $this->sendSuccess('Interest deleted successfully');
    }
}
