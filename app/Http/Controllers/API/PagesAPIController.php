<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreatePagesAPIRequest;
use App\Http\Requests\API\UpdatePagesAPIRequest;
use App\Models\Pages;
use App\Models\Post;
use App\Models\User;
use App\Models\Language;
use App\Models\Interest;
use App\Models\Action;
use App\Models\Activities;
use App\Models\Story;
use App\Models\Banner;
use App\Models\LikeComment;
use App\Models\Tag;
use App\Models\Share;
use App\Models\Leaderboard;
use App\Repositories\PagesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;
use Carbon\Carbon;
/**
 * Class PagesController
 * @package App\Http\Controllers\API
 */

class PagesAPIController extends AppBaseController
{
    /** @var  PagesRepository */
    private $pagesRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use CommonFunctionTrait;

    public function __construct(PagesRepository $pagesRepo)
    {
        $this->pagesRepository = $pagesRepo;
    }

    public function sharePost(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $checkShare = Share::where('post_id', $request->post_id)
                ->where('user_id', $existingUser->id)->first();
                if ($checkShare) {
                    return response()->json(['status'=>'1','message'=>'Post Shared On Timeline'], $this->successStatus);
                }else{
                    $post= new Share();
                    $post->user_id = $existingUser->id;
                    $post->post_id = $request->post_id;
                    $post->save();
                    return response()->json(['status'=>'1','message'=>'Post Shared On Timeline'], $this->successStatus);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function createPage(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'languages' => 'required',
            'interests' => 'required',
            'page_title' => 'required',
            'cover_photo' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                if($request->hasFile('cover_photo')){
                $cover_photo = time().$request->cover_photo->getClientOriginalName();
                $request->cover_photo->move(public_path('/pages_images') . '/', $cover_photo);
                }else{
                    $cover_photo = 'default.jpg';
                }

                if($request->hasFile('background_photo')){
                $background_photo = time().$request->background_photo->getClientOriginalName();
                $request->background_photo->move(public_path('/pages_images') . '/', $background_photo);
                }else{
                    $background_photo = 'default.jpg';
                }

                $languages = array($saveArray['languages']);
                $a = str_replace(array( '[', ']' ), '', $languages);
                $lang = implode(",", $a);
                $langs = explode(",", $lang);
                $languagesids = Language::whereIn('language',$langs)->get();
                $larr = json_decode($languagesids,true);
                $lidarr = array_column($larr,'id');
                $lval = implode(',',$lidarr);

                $activity = array($saveArray['interests']);
                $b = str_replace(array( '[', ']' ), '', $activity);
                $act = implode(",", $b);
                $acts = explode(",", $act);
                $interestsids = Interest::whereIn('interset_name',$acts)->get();
                $arr = json_decode($interestsids,true);
                $idarr = array_column($arr,'id');
                $val = implode(',',$idarr);
                // print_r($lval);die();
                
                $createPage = [
                    'user_id'=>$existingUser['id'],
                    'page_title'=>$saveArray['page_title'],
                    'languages' => $lval,
                    'interests' => $val,
                    'cover_photo'=>$cover_photo,
                    'background_photo'=>$background_photo,
                    'created_at' => date('Y-m-d h:i:s')
                ];
                $insert =  Pages::insertGetId($createPage);
                if ($insert) {
                    return response()->json(['status'=>'1','message'=>'Page Created'], $this->successStatus);
                }else{
                    return response()->json(['message'=>'Something went wrong','status'=>'0'], $this->badrequest);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function shareStory(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'post_media' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $data = [];
                if($request->hasfile('post_media')){
                    foreach($request->file('post_media') as $key=>$file){
                        $name= time().$file->getClientOriginalName();    
                        $file->move(public_path('/post_media') . '/', $name);     
                        $data[$key] = $name;
                    }
                }
                $file= new Story();
                $imgs = implode(",", $data);
                $file->user_id = $existingUser->id;
                $file->media = $imgs;
                $file->content = $request->content?$request->content:'';
                $file->end_time = date('Y-m-d');
                $file->save();

                 return response()->json(['status'=>'1','message'=>'Story Created'], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function createInterest(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'interset_name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $existingInterest = Interest::where('interset_name', $request->interset_name)->first();
                if ($existingInterest) {
                    return response()->json(['status'=>'1','message'=>'Interest Already Created'], $this->successStatus);
                }else{
                    // $input = $request->all();
                    // if($request->hasfile('image')){
                    //     dd("dd");
                    //     $imageName = time().$request->image->getClientOriginalName();
                    //     $request->image->move(public_path('interest'), $imageName);
                    //     $input['image'] = $imageName;
                    // }else{
                    //     $input['image'] = '';
                    // }
                    // $input= new Interest();
                    // $input->interset_name = $request->interset_name;
                    // $input->image = $input['image'];
                    // $input->save();
                    // $data = [];
                    // if($request->hasfile('image'))
                    // {
                    //     dd("dd");
                    //     foreach($request->file('image') as $key=>$file){
                    //         $name= time().$file->getClientOriginalName();    
                    //         $file->move(public_path('/interest') . '/', $name);     
                    //         $data[$key] = $name;  
                    //     }
                    // }

                     $file= new Interest();
                     // $imgs = implode(",", $data);
                     $file->interset_name = $request->interset_name;
                     $file->image = '';
                     $file->save();
                }
                 return response()->json(['status'=>'1','message'=>'Interest Created'], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function createPost(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'page_id' => 'required',
            // 'captions' => 'required',
            'post_media' => 'required',
            'type' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $tags = '';
                if (!empty($saveArray['tags'])) {
                    $gettags = $saveArray['tags'];
                    $alltags = str_replace(array( '[', ']' ), '', $gettags);
                    $tagNames = explode(",",$alltags);
                    $tagIds = [];
                    foreach($tagNames as $tagName)
                    {
                        $tag = Tag::firstOrCreate(['tag'=>$tagName]);
                        if($tag)
                        {
                          $tagIds[] = $tag->id;
                        }

                    }
                    $tag_ids = str_replace(array( '[', ']' ), '', $tagIds);
                    $tags = implode(",", $tag_ids);
                }
                $allUsers = '';
                if (!empty($saveArray['tagged_users'])) {
                    $taggedUsers = $saveArray['tagged_users'];
                    $allUsers = str_replace(array( '[', ']' ), '', $taggedUsers);
                }
                $data = [];
                if($request->hasfile('post_media')){
                    foreach($request->file('post_media') as $key=>$file){
                        $name = time().$file->getClientOriginalName();
                        if ($saveArray['type'] == 'P') {
                            $file->move(public_path('/post_media') . '/', $name);
                        }elseif($saveArray['type'] == 'S'){
                            $file->move(public_path('/post_media') . '/', $name);
                        }elseif($saveArray['type'] == 'B'){
                            $one = $file->move(public_path('/post_media') . '/', $name);
                        }
                        $extension = $file->getClientOriginalExtension();
                        if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
                            $mediatype = 'Image';
                        }elseif ($extension == 'mp4') {
                            $mediatype = 'Video';
                        }else{
                            $mediatype = 'Other';
                        }
                        $data[$key] = $name;
                    }
                    if ($saveArray['type'] == 'P') {
                        $file = new Post();
                        $imgs = implode(",", $data);
                        // print_r($imgs);die();
                        $file->page_id = $request->page_id;
                        $file->post_desc = $request->captions?$request->captions:'';
                        $file->tags = $tags?$tags:'';
                        $file->type = $request->type;
                        $file->mediatype = $mediatype;
                        $file->tagged_users = $allUsers;
                        $file->media = $imgs;
                        $file->save();
                        return response()->json(['status'=>'1','message'=>'Post Created'], $this->successStatus);
                    }elseif($saveArray['type'] == 'S'){
                        $file= new Story();
                        $imgs = implode(",", $data);
                        $file->user_id = $existingUser->id;
                        $file->page_id = $request->page_id;
                        $file->media = $imgs;
                        $file->content = $request->content?$request->content:'';
                        $file->end_time = date('Y-m-d');
                        $file->save();
                        return response()->json(['status'=>'1','message'=>'Story Created'], $this->successStatus);
                    }elseif($saveArray['type'] == 'B'){
                        $file = new Post();
                        $imgs = implode(",", $data);
                        $file->page_id = $request->page_id;
                        $file->post_desc = $request->captions?$request->captions:'';
                        $file->tags = $tags?$tags:'';
                        $file->type = $request->type;
                        $file->mediatype = $mediatype;
                        $file->tagged_users = $allUsers;
                        $file->media = $imgs;
                        $file->save();

                        $file= new Story();
                        $imgs = implode(",", $data);
                        $file->user_id = $existingUser->id;
                        $file->page_id = $request->page_id;
                        $file->media = $imgs;
                        $file->content = $request->content?$request->content:'';
                        $file->end_time = date('Y-m-d');
                        $file->save();
                    }
                    return response()->json(['status'=>'1','message'=>'Post and Story Created'], $this->successStatus);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function editPage(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'page_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExists = User::where('remember_token',$token)->first();
            if ($userExists) {
                $checkPage = Pages::where('id',$saveArray['page_id'])
                ->where('user_id',$userExists['id'])
                ->first();
                if ($checkPage) {
                    if($request->hasFile('cover_photo')){
                        $cover_photo= time().$request->cover_photo->getClientOriginalName();
                        $request->cover_photo->move(public_path('/pages_images') . '/', $cover_photo);
                    }else{
                        $cover_photo = $checkPage['cover_photo'];
                    }

                    if($request->hasFile('background_photo')){
                        $background_photo= time().$request->background_photo->getClientOriginalName();
                        $request->background_photo->move(public_path('/pages_images') . '/', $background_photo);
                    }else{
                        $background_photo = $checkPage['background_photo'];
                    }
                    
                    if (!empty($saveArray['languages'])) {
                        $languages = array($saveArray['languages']);
                        $a = str_replace(array( '[', ']' ), '', $languages);
                        $lang = implode(",", $a);
                        $langs = explode(",", $lang);
                        $languagesids = Language::whereIn('language',$langs)->get();
                        $larr = json_decode($languagesids,true);
                        $lidarr = array_column($larr,'id');
                        $lval = implode(',',$lidarr);
                    }else{
                        $lval = $checkPage['languages'];
                    }
                    
                    if (!empty($saveArray['interests'])) {
                        $activity = array($saveArray['interests']);
                        $b = str_replace(array( '[', ']' ), '', $activity);
                        $act = implode(",", $b);
                        $acts = explode(",", $act);
                        $interestsids = Interest::whereIn('interset_name',$acts)->get();
                        $arr = json_decode($interestsids,true);
                        $idarr = array_column($arr,'id');
                        $val = implode(',',$idarr);
                    }else{
                        $val = $checkPage['interests'];
                    }

                    $updatePage = [
                        'page_title'=>$saveArray['page_title']?$saveArray['page_title']:$checkPage['page_title'],
                        'cover_photo'=>$cover_photo,
                        'background_photo'=>$background_photo,
                        'languages' => $lval,
                        'interests' => $val,
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Pages::where('id',$saveArray['page_id'])
                    ->where('user_id',$userExists['id'])
                    ->update($updatePage);
                    if ($update) {
                        return response()->json(['message'=>'Page Updated','status'=>'1'], $this->successStatus);
                    }
                }else{
                    return response()->json(['message'=>'You cant update this page','status'=>'0'], $this->successStatus);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function deleteStory(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'story_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token',$token)->first();
            if ($existingUser) {
                $getPages = Story::where('id',$saveArray['story_id'])
                ->first();
                // print_r($getPages);die();
                $updateStatus = ['story_status'=>'deleted'];
                $update = Story::where('id',$saveArray['story_id'])->update($updateStatus);
                if ($update) {
                    return response()->json(['message'=>'Story deleted','status'=>'1'
                    ], $this->successStatus);
                }else{
                    return response()->json(['message'=>'Something went wrong','status'=>'0'
                    ], $this->badrequest);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                    ], $this->badrequest);
            }
        }
    }

    public function deletePost(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token',$token)->first();
            if ($existingUser) {
                $getPost = Post::where('id',$saveArray['post_id'])->first();
                $updateStatus = ['status'=>'2'];
                $update = Post::where('id',$saveArray['post_id'])->update($updateStatus);
                if ($update) {
                    return response()->json(['message'=>'Post deleted','status'=>'1'
                    ], $this->successStatus);
                }else{
                    return response()->json(['message'=>'Something went wrong','status'=>'0'
                    ], $this->badrequest);
                }
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                    ], $this->badrequest);
            }
        }
    }

    public function test(Request $request){
        $ports = collect([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);

        $ads = 'ad';

        $port = $ports->chunk(4)->each->push($ads)->collapse();
        print_r($port);die();
    }

    public function leaderBoard(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            // 'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token',$token)->first();
            if ($existingUser) {
                $getRandom = User::where('type','!=','A')
                ->select('id','name','image')
                ->inRandomOrder()
                ->take(15)
                ->get();
                // ->limit(21);
                $getLeaderboard = leaderboard::orderBy('id','DESC')->first();
                // print_r($getLeaderboard['notes']);die();
                // foreach($stories AS $media){
                //     $storymedia = explode(",", $media['media']);
                //     foreach ($storymedia as $med) {
                //         $slist['id'] = $media['id'];
                //         $surl = url("/post_media/".$med);
                //         $slist['post_media'] = $surl;
                //         $storyArr[] = $slist;
                //     }
                    
                // }
                $userArr = [];
                $i = 1;
                $points = 60;
                foreach($getRandom as $key=>$user)
                {
                        $list['rank'] = $i++;
                        $list['name'] = $user['name'];
                        $list['image'] = url('/').'/user_images/'.$user['image'];
                        $list['points'] = $points++;
                        $userArr[] = $list;
                }
                $string = trim(preg_replace('/\s\s+/', ',', $getLeaderboard['notes']));
                return response()->json([
                    'status'=>'1',
                    'message'=>'Users fetched successfully',
                    'start_date' => $getLeaderboard['start_day']->format('Y-m-d h:m:i'),
                    'end_date' => $getLeaderboard['end_day']->format('Y-m-d h:m:i'),
                    'rules' => array($string),
                    'leaderboard' => array_reverse($userArr)
                ]);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                    ], $this->badrequest);
        }
        }
    }

    public function homeListing(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if ($existingUser) {

            $getRecentStory = Story::where('story_status','active')
            ->where("created_at",">",Carbon::now()->subDay())
            ->select('page_id')
            ->latest('id')
            ->get()->toArray();
            $array = array_column($getRecentStory, 'page_id');
            $result = array_unique($array);
            $getUsers = Pages::whereIn('id',$result)->select('id','page_title','cover_photo')->get()->toArray();
            $storyArray = [];
            foreach ($getUsers as $user) {
                $uimg = asset('/pages_images/' . $user['cover_photo']);
                $slist['id'] = $user['id'];
                $slist['name'] = $user['page_title'];
                $slist['image'] = $uimg;
                $storyArray[] = $slist;
            }
            $storyArr = $storyArray;

            $banner = Banner::all()->pluck('image');
            foreach($banner AS $index => $image){
              $banner[$index] = url("/banner/".$image);
            }

            if ($request->type == 'T') { #timeline
            $posts = Action::where('user_id',$existingUser->id)
            ->where('object_type','PAGE')
            ->where('status','followed')
            ->select('object_id')->get()->toArray();
            $posts = Post::whereIn('page_id',$posts)
            ->where('status','1')
            ->where('type','P')
            ->orWhere('type','B')
            ->inRandomOrder()
            ->get()
            ->toArray();
            $postArr = [];
            foreach ($posts as $post ) {
                $pagematchThese = ['user_id' => $existingUser['id'],'object_id' => $post['page_id'],'object_type' => 'PAGE','status' => 'followed'];
                $pfollowStatus = Action::where($pagematchThese)->first();
                if ($pfollowStatus) {
                    $isFollowed = "Yes";
                }else{
                    $isFollowed = "No";
                }
                /*get Page followers list*/
                $type = 'P';
                $getUserFollowers = $this->followers($type, $post['page_id']);
                $pageArr = array();
                foreach ($getUserFollowers as $follower) {
                    $user1 = User::where('id',$follower['user_id'])->first();
                    $uimage1 = asset('/user_images/' . $user1['image']);
                    $follow['user_id'] = $follower['user_id'];
                    $follow['name'] = $user1['name'];
                    $follow['image'] = $uimage1;
                    $pageArr[] = $follow;
                }
                /*get Page followers list*/
                $isLiked = LikeComment::where('post_id',$post['id'])
                ->where('user_id',$existingUser['id'])
                ->where('isliked','!=','')
                ->select('isliked')
                ->first();
                if ($isLiked == '') {
                    $islike = 'No';
                }else{
                    $islike = $isLiked['isliked'];
                }
                /*get likes count*/
                $getLikes = LikeComment::where('post_id',$post['id'])
                ->where('isliked','!=','')
                ->get();
                $getthumbLikes = $this->thumblikesCount($post['id']);
                $getHeartLikes = $this->heartlikesCount($post['id']);
                $getSmileLikes = $this->smilelikesCount($post['id']);
                $getFlowerLikes = $this->flowerlikesCount($post['id']);
                /*get likes count*/
                $getComments = LikeComment::where('post_id',$post['id'])
                ->where('comment','!=','')
                ->get();
                $media = explode(",", $post['media']);
                $mediaArray = [];
                foreach ($media as $med) {
                    $media = asset('/post_media/' . $med);
                    $medi['media'] = $media;
                    $mediaArray[] = $medi;
                }
                $page = Pages::where('id',$post['page_id'])->first();
                $user = User::where('id',$page['user_id'])->first();
                $cover = asset('/pages_images/' . $page['cover_photo']);
                $list['createdBy_id'] = $page['user_id'];
                $list['createdBy_name'] = $user['name'];
                $list['page_id'] = (string)$post['page_id'];
                $list['page_followers'] = $pageArr;
                $list['page_followers_count'] = count($pageArr);
                $list['page_followed'] = $isFollowed;
                $list['page_title'] = $page['page_title'];
                $list['page_image'] = $cover;
                $list['post_id'] = (string)$post['id'];
                $list['post_desc'] = $post['post_desc'];
                $list['media'] = $mediaArray;
                $list['likes_count'] = count($getLikes);
                $list['thumbs_count'] = count($getthumbLikes);
                $list['heart_count'] = count($getHeartLikes);
                $list['smile_count'] = count($getSmileLikes);
                $list['flower_count'] = count($getFlowerLikes);
                // $list['likes'] = $getLikes;
                $list['comments_count'] = count($getComments);
                // $list['comments'] = $getComments;
                $list['isLike'] = $islike;
                $postArr[] = $list;
            }
            return response()->json([
                'status'=>'1',
                'message'=>'homeposts fetched successfully',
                'banners'=>$banner,
                'stories'=>$storyArr,
                'posts'=>$postArr
            ], $this->successStatus);
            }elseif ($request->type == 'N') { #new
                $userActs = Activities::where('user_id',$existingUser->id)
                ->where('interests','!=','')
                ->first();
                if ($userActs) {
                    $query = Pages::query();
                    $tags = explode(",",$userActs['interests']);
                    $query = $query->where(function($query) use($tags) {
                        foreach($tags as $tag) {
                            $query->orWhere('interests', 'like', "%$tag%");
                        };
                    });
                    // ->inRandomOrder();
                    $pages = $query->select('id')->get()->toArray();
                    // print_r($pages);die();
                        $posts = Post::whereIn('page_id',$pages)
                        ->where('status','1')
                        ->where('type','P')
                        ->orWhere('type','B')
                        ->orderBy('created_at','DESC')
                        ->get()->toArray();
                        $postArr = [];
                        foreach ($posts as $post ) {
                            $pagematchThese = ['user_id' => $existingUser['id'],'object_id' => $post['page_id'],'object_type' => 'PAGE','status' => 'followed'];
                            $pfollowStatus = Action::where($pagematchThese)->first();
                            if ($pfollowStatus) {
                                $isFollowed = "Yes";
                            }else{
                                $isFollowed = "No";
                            }
                            /*get Page followers list*/
                            $type = 'P';
                            $getUserFollowers = $this->followers($type, $post['page_id']);
                            $pageArr = array();
                            foreach ($getUserFollowers as $follower) {
                                $user1 = User::where('id',$follower['user_id'])->first();
                                $uimage1 = asset('/user_images/' . $user1['image']);
                                $follow['user_id'] = $follower['user_id'];
                                $follow['name'] = $user1['name'];
                                $follow['image'] = $uimage1;
                                $pageArr[] = $follow;
                            }
                            /*get Page followers list*/
                            $isLiked = LikeComment::where('post_id',$post['id'])
                            ->where('user_id',$existingUser['id'])
                            ->where('isliked','!=','')
                            ->select('isliked')
                            ->first();
                            if ($isLiked == '') {
                                $islike = 'No';
                            }else{
                                $islike = $isLiked['isliked'];
                            }
                            $getLikes = LikeComment::where('post_id',$post['id'])
                            ->where('isliked','!=','')
                            ->get();
                            $getthumbLikes = $this->thumblikesCount($post['id']);
                            $getHeartLikes = $this->heartlikesCount($post['id']);
                            $getSmileLikes = $this->smilelikesCount($post['id']);
                            $getFlowerLikes = $this->flowerlikesCount($post['id']);
                            $getComments = LikeComment::where('post_id',$post['id'])
                            ->where('comment','!=','')
                            ->get();
                            $media = explode(",", $post['media']);
                            $mediaArray = [];
                            foreach ($media as $med) {
                                $media = asset('/post_media/' . $med);
                                $medi['media'] = $media;
                                $mediaArray[] = $medi;
                            }
                            $page = Pages::where('id',$post['page_id'])->first();
                            $user = User::where('id',$page['user_id'])->first();
                            $cover = asset('/pages_images/' . $page['cover_photo']);
                            $list['createdBy_id'] = $page['user_id'];
                            $list['createdBy_name'] = $user['name']?$user['name']:'';
                            $list['page_id'] = (string)$post['page_id'];
                            $list['page_followers'] = $pageArr;
                            $list['page_followers_count'] = count($pageArr);
                            $list['page_followed'] = $isFollowed;
                            $list['page_title'] = $page['page_title'];
                            $list['page_image'] = $cover;
                            $list['post_id'] = (string)$post['id'];
                            $list['post_desc'] = $post['post_desc'];
                            $list['media'] = $mediaArray;
                            $list['likes_count'] = count($getLikes);
                            $list['thumbs_count'] = count($getthumbLikes);
                            $list['heart_count'] = count($getHeartLikes);
                            $list['smile_count'] = count($getSmileLikes);
                            $list['flower_count'] = count($getFlowerLikes);
                            // $list['likes'] = $getLikes;
                            $list['comments_count'] = count($getComments);
                            // $list['comments'] = $getComments;
                            $list['isLike'] = $islike;
                            $postArr[] = $list;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'homeposts fetched successfully',
                            'banners'=>$banner,
                            'stories'=>$storyArr,
                            'posts'=>$postArr
                        ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'homeposts fetched successfully',
                        'posts'=>[]
                    ], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'User not found','status'=>'0'], $this->badrequest);
        }
    }

    public function getStorys(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'page_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = User::where('remember_token', $token)->first();
            if ($existingUser) {
                $stories = Story::where('page_id',$saveArray['page_id'])
                ->where('story_status','active')
                ->where("created_at",">",Carbon::now()->subDay())
                ->get();
                $storyArr = [];
                foreach($stories AS $media){
                    $storymedia = explode(",", $media['media']);
                    foreach ($storymedia as $med) {
                        $slist['id'] = $media['id'];
                        $surl = url("/post_media/".$med);
                        $slist['post_media'] = $surl;
                        $storyArr[] = $slist;
                    }
                    
                }
                return response()->json(['status'=>'1','message'=>'story fetched successfully','stories'=>$storyArr], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function myPages(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if ($existingUser) {
        $pages = Pages::where('user_id', $existingUser['id'])
        ->orderBy('created_at','DESC')
        ->get();
        $pageArr = [];
        foreach ($pages as $page) {
            $cover_photo = asset('/pages_images/' . $page['cover_photo']);
            $background_photo = asset('/pages_images/' . $page['background_photo']);
            $posts = Post::where('page_id',$page['id'])
            ->where('status','1')
            ->orderBy('created_at','DESC')
            ->get();
            $postArr = [];
            foreach ($posts as $post ) {
                $getLikes = LikeComment::where('post_id',$post['id'])
                ->where('isliked','!=','')
                ->get();
                $media = explode(",", $post['media']);
                $mediaArray = [];
                foreach ($media as $med) {
                    $media = asset('/post_media/' . $med);
                    $medi['media'] = $media;
                    $mediaArray[] = $medi;
                }
                $lists['post_id'] = (string)$post['id'];
                $lists['post_desc'] = $post['post_desc'];
                $lists['media'] = $mediaArray;
                $lists['likes'] = $getLikes;
                $postArr[] = $lists;
            }
            $list['page_id'] = (string)$page['id'];
            $list['page_title'] = $page['page_title'];
            $list['cover_photo'] = $cover_photo;
            $list['background_photo'] = $background_photo;
            $list['createdBy_id'] = $page['user_id'];
            $list['posts'] = $postArr;
            $pageArr[] = $list;
        }
        return response()->json([
            'status'=>'1',
            'message'=>'pages fetched successfully',
            'mypages'=>$pageArr
        ], $this->successStatus);
        }else{
            return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function getPage(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'page_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExists = User::where('remember_token',$token)->first();
            if ($userExists) {
                $pageOwn = Pages::where('id',$saveArray['page_id'])->first();
                $lang = explode(",", $pageOwn['languages']);
                $getLangs = Language::whereIn('id',$lang)->get();
                $langArray = [];
                foreach ($getLangs as $value) {
                    $listing['id'] = (string)$value['id'];
                    $listing['language'] = $value['language'];
                    $langArray[] = $listing;
                }
                $type = 'P';
                $pageFollowers = $this->followers($type, $saveArray['page_id']);
                $followerArr = array();
                foreach ($pageFollowers as $follower) {
                    $user1 = User::where('id',$follower['user_id'])->first();
                    $uimage1 = asset('/user_images/' . $user1->image);
                    $follow['user_id'] = $follower['user_id'];
                    $follow['name'] = $user1['name'];
                    $follow['image'] = $uimage1;
                    $followerArr[] = $follow;
                }
                $pagematchThese = ['user_id' => $userExists['id'],'object_id' => $saveArray['page_id'],'object_type' => 'PAGE','status' => 'followed'];
                $pfollowStatus = Action::where($pagematchThese)->first();
                if ($pfollowStatus) {
                    $isFollowed = "Yes";
                }else{
                    $isFollowed = "No";
                }
                $cover_photo = asset('/pages_images/' . $pageOwn['cover_photo']);
                $background_photo = asset('/pages_images/' . $pageOwn['background_photo']);
                $posts = Post::where('page_id',$pageOwn['id'])
                ->where('status','1')
                ->orderBy('created_at','DESC')
                ->get();
                $inter = explode(",", $pageOwn['interests']);
                $getInter = Interest::whereIn('id',$inter)->get();
                $interestArray = [];
                foreach($getInter as $interest){
                    $intimage = url('/interest',$interest['image']);
                    $listing['id'] = (string)$interest['id'];
                    $listing['Interset'] = $interest['interset_name'];
                    $listing['image'] = $intimage;
                    $interestArray[] = $listing;
                }
                $ownerData = User::where('id',$pageOwn['user_id'])->first();
                $pagedata = array(
                    "posts_count" => (string)count($posts),
                    "followers_count" => (string)count($pageFollowers),
                    "followers" => $followerArr,
                    "page_id" => $pageOwn['id'],
                    "owner_id" => $pageOwn['user_id'],
                    "owner_detail" => $ownerData,
                    "page_title" => $pageOwn['page_title'],
                    "cover_photo" => $cover_photo,
                    "background_photo" => $background_photo,
                    "createdBy_id" => $pageOwn['user_id'],
                    "createdBy_name" => $userExists['name'],
                    "isFollowed" => $isFollowed,
                    "page_interests" => $interestArray,
                    "page_languages" => $langArray
                );
                $postArr = [];
                foreach ($posts as $post ) {
                    $getLikes = LikeComment::where('post_id',$post['id'])
                    ->where('isliked','!=','')
                    ->get();
                    $getComments = LikeComment::where('post_id',$post['id'])
                    ->where('comment','!=','')
                    ->get();
                    /*likes overall*/
                    $getthumbLikes = $this->thumblikesCount($post['id']);
                    $getHeartLikes = $this->heartlikesCount($post['id']);
                    $getSmileLikes = $this->smilelikesCount($post['id']);
                    $getFlowerLikes = $this->flowerlikesCount($post['id']);
                    /*likes overall*/
                    $media = explode(",", $post['media']);
                    $mediaArray = [];
                    foreach ($media as $med) {
                        $media = asset('/post_media/' . $med);
                        $medi['media'] = $media;
                        $mediaArray[] = $medi;
                    }
                    $lists['post_id'] = (string)$post['id'];
                    $lists['post_desc'] = $post['post_desc'];
                    $lists['likes_count'] = count($getLikes);
                    $lists['likes'] = $getLikes;
                    $lists['comments_count'] = count($getComments);
                    $lists['comments'] = $getComments;
                    $lists['media'] = $mediaArray;
                    $lists['thumbs_count'] = count($getthumbLikes);
                    $lists['heart_count'] = count($getHeartLikes);
                    $lists['smile_count'] = count($getSmileLikes);
                    $lists['flower_count'] = count($getFlowerLikes);
                    $postArr[] = $lists;
                }
                $stories = Story::where('page_id',$saveArray['page_id'])
                ->where('story_status','active')
                ->get();
                $storyArr = [];
                foreach($stories AS $media){
                    $storymedia = explode(",", $media['media']);
                    foreach ($storymedia as $med) {
                        $slist['id'] = $media['id'];
                        $surl = url("/post_media/".$med);
                        $slist['post_media'] = $surl;
                        $storyArr[] = $slist;
                    }
                    
                }
                // print_r($storyArr);die();
                return response()->json([
                    'status'=>'1',
                    'message'=>'page fetched successfully',
                    'pageDetail'=>$pagedata,
                    'storyArr'=>$storyArr,
                    'myposts'=>$postArr
                ], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function getPost(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExists = User::where('remember_token',$token)->first();
            if ($userExists) {
                $posts = Post::where('id',$saveArray['post_id'])
                ->where('status','1')
                ->first();
                $likeArr = [];
                $getLikes = LikeComment::where('post_id',$saveArray['post_id'])
                ->where('isliked','!=','')
                ->get();
                foreach ($getLikes as $like) {
                    $user = User::where('id',$like['user_id'])->first();
                    $usermedia = asset('/user_images/' . $user['image']);
                    $list['user_id'] = (string)$like['user_id'];
                    $list['user_name'] = $user['name'];
                    $list['profile_pic'] = $usermedia;
                    $list['isliked'] = $like['isliked'];
                    $likeArr[] = $list;
                }
                $commentArr = [];
                $getComments = LikeComment::where('post_id',$saveArray['post_id'])
                ->where('comment','!=','')
                ->get();
                foreach ($getComments as $comment) {
                    $user = User::where('id',$comment['user_id'])->first();
                    $usermedia = asset('/user_images/' . $user['image']);
                    $lists['user_id'] = (string)$comment['user_id'];
                    $lists['user_name'] = $user['name'];
                    $lists['profile_pic'] = $usermedia;
                    $lists['comment'] = $comment['comment'];
                    $lists['created_at'] = $comment['created_at']->format('d-m-Y');
                    $commentArr[] = $lists;
                }
                $media = explode(",", $posts['media']);
                $mediaArray = [];
                foreach ($media as $med) {
                    $media = asset('/post_media/' . $med);
                    $medi['media'] = $media;
                    $mediaArray[] = $medi;
                }
                /*likes overall*/
                $getthumbLikes = $this->thumblikesCount($saveArray['post_id']);
                $getHeartLikes = $this->heartlikesCount($saveArray['post_id']);
                $getSmileLikes = $this->smilelikesCount($saveArray['post_id']);
                $getFlowerLikes = $this->flowerlikesCount($saveArray['post_id']);
                /*likes overall*/
                $postArr = [
                    'post_id' => (string)$posts['id'],
                    'post_desc' => $posts['post_desc'],
                    'media' => $mediaArray,
                    'likes_count' => count($getLikes),
                    'likes' => $likeArr,
                    'comments_count' => count($getComments),
                    'comments' => $commentArr,
                    'thumbs_count' => count($getthumbLikes),
                    'heart_count' => count($getHeartLikes),
                    'smile_count' => count($getSmileLikes),
                    'flower_count' => count($getFlowerLikes)
                ];
                return response()->json([
                    'status'=>'1',
                    'message'=>'post fetched successfully',
                    'postDetails'=>$postArr
                ], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'], $this->badrequest);
            }
        }
    }

    public function likeComments(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'liked' => 'required_without_all:comment,comment_reply',
            'comment' => 'required_without_all:liked,comment_reply',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $userExists = User::where('remember_token',$token)->first();
            if ($userExists) {
                $checkPost = Post::where('id',$saveArray['post_id'])
                ->where('status','1')
                ->first();
                if ($checkPost) {
                    if ($saveArray['liked'] == '1' || $saveArray['liked'] == '2' || $saveArray['liked'] == '3' || $saveArray['liked'] == '4' || $saveArray['liked'] == '5') {
                        $checkLike = LikeComment::where('post_id',$saveArray['post_id'])
                        ->where('user_id',$userExists['id'])
                        ->first();
                        if (!empty($checkLike)) {
                            //update like emoji or dislike
                            if ($saveArray['liked'] == '5') {
                                $update = LikeComment::where('post_id',$saveArray['post_id'])
                                ->where('user_id',$userExists['id'])
                                ->delete();
                            return response()->json(['status' => '1','message'=>'disliked successfully'], $this->successStatus);
                            }elseif ($saveArray['liked'] == '1' || $saveArray['liked'] == '2' || $saveArray['liked'] == '3' || $saveArray['liked'] == '4') {
                                $updateEmoji = [
                                    'isliked'=>$saveArray['liked']
                                ];
                                $update = LikeComment::where('post_id',$saveArray['post_id'])
                                ->where('user_id',$userExists['id'])
                                ->update($updateEmoji);
                                return response()->json(['status'=>'1','message'=>'Post liked'], $this->successStatus);
                            }
                        }else{
                            $likePost = [
                                'user_id'=>$userExists['id'],
                                'post_id'=>$saveArray['post_id'],
                                'isliked'=>$saveArray['liked'],
                                'created_at' => date('Y-m-d h:i:s')
                            ];
                            $insert =  LikeComment::insertGetId($likePost);
                            /*fire follow notification*/
                                $pageDetails = Pages::where('id',$checkPost['page_id'])
                                ->first();
                                // print_r($pageDetails);die();
                                $deviceIds = User::where('id',$pageDetails['user_id'])
                                ->select('id','device_id','device_type','username')
                                ->first();
                                $deviceArr = $deviceIds['device_id'];
                                $message = ''.$deviceIds['username'].' liked your post';
                                if ($deviceIds['device_type'] == 'ANDROID') {
                                    $followNoti = $this->android_send_notification($deviceArr,$message,$pageDetails['user_id']);
                                }else{
                                    $followNoti = $this->iphone_send_notifications($deviceArr,$message,$pageDetails['user_id']);
                                }

                                $notificationArr = array(
                                    'user_id'=> $userExists['id'],
                                    'object_id'=> $pageDetails['user_id'],
                                    'message'=> $message,
                                    'act'=> "POST_LIKE",
                                    'read_status'=> "0",
                                );

                                $saveNoti = $this->storeNotification($notificationArr);
                            /*fire follow notification*/
                            if ($insert) {
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'liked successfully',
                                ], $this->successStatus);
                            }
                        }
                    }elseif (!empty($saveArray['comment'])) {
                        $addCommebt = [
                            'user_id'=>$userExists['id'],
                            'post_id'=>$saveArray['post_id'],
                            'comment'=>$saveArray['comment'],
                            'created_at' => date('Y-m-d h:i:s')
                        ];
                        $insert =  LikeComment::insertGetId($addCommebt);
                        /*fire follow notification*/
                            $pageDetails = Pages::where('id',$checkPost['page_id'])
                                ->first();
                                // print_r($pageDetails);die();
                            $deviceIds = User::where('id',$pageDetails['user_id'])
                            ->select('id','device_id','device_type','username')
                            ->first();
                             // print_r($deviceIds);die();
                            $deviceArr = $deviceIds['device_id'];
                            $message = ''.$userExists['username'].' commented your post';
                            if ($deviceIds['device_type'] == 'ANDROID') {
                                $followNoti = $this->android_send_notification($deviceArr,$message,$pageDetails['user_id']);
                            }else{
                                $followNoti = $this->iphone_send_notifications($deviceArr,$message,$pageDetails['user_id']);
                            }

                            $notificationArr = array(
                                'user_id'=> $userExists['id'],
                                'object_id'=> $pageDetails['user_id'],
                                'message'=> $message,
                                'act'=> "POST_COMMENT",
                                'read_status'=> "0",
                            );

                            $saveNoti = $this->storeNotification($notificationArr);
                        /*fire follow notification*/
                        if ($insert) {
                            return response()->json([
                                'status'=>'1',
                                'message'=>'comment added successfully',
                            ], $this->successStatus);
                        }
                    }elseif (!empty($saveArray['comment_reply'])) {
                        return response()->json(['status'=>'1','message'=>'future scope',], $this->successStatus);
                    }else{
                        return response()->json(['status'=>'1','message'=>'Invalid Input',], $this->successStatus);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'Post not found',], $this->successStatus);
                }
            }else{
                return response()->json(['status'=>'1','message'=>'User not found',], $this->badrequest);
            }
        }
    }

    public function menuPages(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where(['remember_token'=>$token])->first();
        $ptype = 'P';
        $getPageFollowings = $this->followings($ptype, $existingUser['id']);
        $followpagesArr = array();
        foreach ($getPageFollowings as $following) {
            $page = Pages::where('id',$following['object_id'])->first();
            $cover_photo = asset('/pages_images/' . $page->cover_photo);
            $background_photo = asset('/pages_images/' . $page->background_photo);
            $pages['page_id'] = $following['object_id'];
            $pages['page_title'] = $page['page_title'];
            $pages['cover_photo'] = $cover_photo;
            $pages['background_photo'] = $background_photo;
            $followpagesArr[] = $pages;
        }
        $trending = Pages::inRandomOrder()->limit(15)->get();
        $trendArr = array();
        foreach ($trending as $trend) {
            $tcover_photo = asset('/pages_images/' . $trend['cover_photo']);
            $tbackground_photo = asset('/pages_images/' . $trend['background_photo']);
            $tpages['page_id'] = $trend['id'];
            $tpages['page_title'] = $trend['page_title'];
            $tpages['cover_photo'] = $tcover_photo;
            $tpages['background_photo'] = $tbackground_photo;
            $trendArr[] = $tpages;
        }
        return response()->json([
            'status'=>'1',
            'message'=>'Pages fetched successfully',
            'followed_pages' => $followpagesArr,
            'trending_pages' => $trendArr
        ]);
    }

    public function randomPosts(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where(['remember_token'=>$token])->first();
        $posts = Post::where('status','1')
        ->where('type','P')
        ->orWhere('type','B')
        ->inRandomOrder()
        ->get()
        ->toArray();
        $postArr = [];
        foreach ($posts as $post ) {
            $pagematchThese = ['user_id' => $existingUser['id'],'object_id' => $post['page_id'],'object_type' => 'PAGE','status' => 'followed'];
            $pfollowStatus = Action::where($pagematchThese)->first();
            if ($pfollowStatus) {
                $isFollowed = "Yes";
            }else{
                $isFollowed = "No";
            }
            /*get Page followers list*/
            $type = 'P';
            $getUserFollowers = $this->followers($type, $post['page_id']);
            $pageArr = array();
            foreach ($getUserFollowers as $follower) {
                $user1 = User::where('id',$follower['user_id'])->first();
                $uimage1 = asset('/user_images/' . $user1['image']);
                $follow['user_id'] = $follower['user_id'];
                $follow['name'] = $user1['name'];
                $follow['image'] = $uimage1;
                $pageArr[] = $follow;
            }
            /*get Page followers list*/
            $isLiked = LikeComment::where('post_id',$post['id'])
            ->where('user_id',$existingUser['id'])
            ->where('isliked','!=','')
            ->select('isliked')
            ->first();
            if ($isLiked == '') {
                $islike = 'No';
            }else{
                $islike = $isLiked['isliked'];
            }
            /*get likes count*/
            $getLikes = LikeComment::where('post_id',$post['id'])
            ->where('isliked','!=','')
            ->get();
            $getthumbLikes = $this->thumblikesCount($post['id']);
            $getHeartLikes = $this->heartlikesCount($post['id']);
            $getSmileLikes = $this->smilelikesCount($post['id']);
            $getFlowerLikes = $this->flowerlikesCount($post['id']);
            /*get likes count*/
            $getComments = LikeComment::where('post_id',$post['id'])
            ->where('comment','!=','')
            ->get();
            $media = explode(",", $post['media']);
            $mediaArray = [];
            foreach ($media as $med) {
                $media = asset('/post_media/' . $med);
                $medi['media'] = $media;
                $mediaArray[] = $medi;
            }
            $page = Pages::where('id',$post['page_id'])->first();
            $user = User::where('id',$page['user_id'])->first();
            $cover = asset('/pages_images/' . $page['cover_photo']);
            $list['createdBy_id'] = $page['user_id'];
            $list['createdBy_name'] = $user['name'];
            $list['page_id'] = (string)$post['page_id'];
            $list['page_followers'] = $pageArr;
            $list['page_followers_count'] = count($pageArr);
            $list['page_followed'] = $isFollowed;
            $list['page_title'] = $page['page_title'];
            $list['page_image'] = $cover;
            $list['post_id'] = (string)$post['id'];
            $list['post_desc'] = $post['post_desc'];
            $list['media'] = $mediaArray;
            $list['likes_count'] = count($getLikes);
            $list['thumbs_count'] = count($getthumbLikes);
            $list['heart_count'] = count($getHeartLikes);
            $list['smile_count'] = count($getSmileLikes);
            $list['flower_count'] = count($getFlowerLikes);
            // $list['likes'] = $getLikes;
            $list['comments_count'] = count($getComments);
            // $list['comments'] = $getComments;
            $list['isLike'] = $islike;
            $postArr[] = $list;
        }
        return response()->json([
            'status'=>'1',
            'message'=>'random fetched successfully',
            'posts'=>$postArr
        ], $this->successStatus);
    }

    public function randomVideos(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where(['remember_token'=>$token])->first();
        $posts = Post::where('mediatype','Video')
        ->where('status','1')
        ->where('type','P')
        ->orWhere('type','B')
        ->inRandomOrder()
        ->get()
        ->toArray();
        // print_r($posts);die();
        $postArr = [];
        foreach ($posts as $post ) {
            $pagematchThese = ['user_id' => $existingUser['id'],'object_id' => $post['page_id'],'object_type' => 'PAGE','status' => 'followed'];
            $pfollowStatus = Action::where($pagematchThese)->first();
            if ($pfollowStatus) {
                $isFollowed = "Yes";
            }else{
                $isFollowed = "No";
            }
            /*get Page followers list*/
            $type = 'P';
            $getUserFollowers = $this->followers($type, $post['page_id']);
            $pageArr = array();
            foreach ($getUserFollowers as $follower) {
                $user1 = User::where('id',$follower['user_id'])->first();
                $uimage1 = asset('/user_images/' . $user1['image']);
                $follow['user_id'] = $follower['user_id'];
                $follow['name'] = $user1['name'];
                $follow['image'] = $uimage1;
                $pageArr[] = $follow;
            }
            /*get Page followers list*/
            $isLiked = LikeComment::where('post_id',$post['id'])
            ->where('user_id',$existingUser['id'])
            ->where('isliked','!=','')
            ->select('isliked')
            ->first();
            if ($isLiked == '') {
                $islike = 'No';
            }else{
                $islike = $isLiked['isliked'];
            }
            /*get likes count*/
            $getLikes = LikeComment::where('post_id',$post['id'])
            ->where('isliked','!=','')
            ->get();
            $getthumbLikes = $this->thumblikesCount($post['id']);
            $getHeartLikes = $this->heartlikesCount($post['id']);
            $getSmileLikes = $this->smilelikesCount($post['id']);
            $getFlowerLikes = $this->flowerlikesCount($post['id']);
            /*get likes count*/
            $getComments = LikeComment::where('post_id',$post['id'])
            ->where('comment','!=','')
            ->get();
            $media = explode(",", $post['media']);
            $mediaArray = [];
            foreach ($media as $med) {
                $media = asset('/post_media/' . $med);
                $medi['media'] = $media;
                $mediaArray[] = $medi;
            }
            $page = Pages::where('id',$post['page_id'])->first();
            $user = User::where('id',$page['user_id'])->first();
            $cover = asset('/pages_images/' . $page['cover_photo']);
            $list['createdBy_id'] = $page['user_id'];
            $list['createdBy_name'] = $user['name'];
            $list['page_id'] = (string)$post['page_id'];
            $list['page_followers'] = $pageArr;
            $list['page_followers_count'] = count($pageArr);
            $list['page_followed'] = $isFollowed;
            $list['page_title'] = $page['page_title'];
            $list['page_image'] = $cover;
            $list['post_id'] = (string)$post['id'];
            $list['post_desc'] = $post['post_desc'];
            $list['media'] = $mediaArray;
            $list['likes_count'] = count($getLikes);
            $list['thumbs_count'] = count($getthumbLikes);
            $list['heart_count'] = count($getHeartLikes);
            $list['smile_count'] = count($getSmileLikes);
            $list['flower_count'] = count($getFlowerLikes);
            // $list['likes'] = $getLikes;
            $list['comments_count'] = count($getComments);
            // $list['comments'] = $getComments;
            $list['isLike'] = $islike;
            $postArr[] = $list;
        }
        return response()->json([
            'status'=>'1',
            'message'=>'Random Videos fetched successfully',
            'posts'=>$postArr
        ], $this->successStatus);
    }
}
