<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVisaAPIRequest;
use App\Http\Requests\API\UpdateVisaAPIRequest;
use App\Models\User;
use App\Models\Visa;
use App\Models\Visa_detail;
use App\Models\Media;
use App\Models\Payment;
use Validator;
use App\Repositories\VisaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class VisaController
 * @package App\Http\Controllers\API
 */

class VisaAPIController extends AppBaseController
{
    /** @var  VisaRepository */
    private $visaRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(VisaRepository $visaRepo)
    {
        $this->visaRepository = $visaRepo;
    }

    /**
     * Display a listing of the Visa.
     * GET|HEAD /visas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $visas = $this->visaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($visas->toArray(), 'Visas retrieved successfully');
    }

    /**
     * Store a newly created Visa in storage.
     * POST /visas
     *
     * @param CreateVisaAPIRequest $request
     *
     * @return Response
     */

    public function applyVisa(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if($existingUser){
            $input = $request->all();
            $newApp =  new \App\Models\Visa;
            $newApp->user_id = $existingUser->id;
            $newApp->type = $request->type;
            $newApp->country_code = $request->country_code;
            $newApp->country = $request->country;
            $newApp->cost = $request->cost;
            $newApp->due_cost = $request->due_cost;
            $newApp->applied_date = $request->applied_date;
            $newApp->payment_type = $request->payment_type;
            $newApp->multi_entry_doc_submission = $request->multi_entry_doc_submission;
            $newApp->save();

            /*visa detail*/
            $visaDetail =  new \App\Models\Visa_detail;
            $visaDetail->user_id = $existingUser->id;
            $visaDetail->visa_id = $newApp->id;
            $visaDetail->name = $request->name;
            $visaDetail->DOB = $request->DOB;
            $visaDetail->passport_no = $request->passport_no;
            $visaDetail->flight_booked = $request->flight_booked;
            $visaDetail->accomodation_booked = $request->accomodation_booked;
            $visaDetail->passanger_count = $request->passanger_count;
            $visaDetail->kids = $request->kids;
            $visaDetail->save();
            /*visa detail*/

            $data = [];
            if($request->hasfile('upload_documents'))
            {
                foreach($request->file('upload_documents') as $key=>$file)
                {
                    $name= time().$file->getClientOriginalName();    
                    $file->move(public_path('/upload_documents') . '/', $name);     
                    $data[$key] = $name;  
                }
            }
            $upload = Visa::where('id',$newApp->id)->update([
                'upload_doc' => $data[0] , 
                'familty_paper' => $data[1], 
                'passport_doc' => $data[2]]);
            // $file= new Visa();
            // $file->visa_id = $newApp->id;
            // $file->user_id = $existingUser->id;
            // $imgs = implode(",", $data);
            // print_r($data);die();
            // $file->media = $imgs;
            // $file->save();
            return response()->json(['status'=>'1','message'=>'Visa applied successfully','visa_id'=>(String)$newApp->id,], $this->successStatus);
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function visaDetail(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if($existingUser){
            $validator = Validator::make($request->all(), [
                'visa_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }else{
                $visa = Visa::where(['id'=>$request->visa_id,'user_id'=>$existingUser->id])->first();
                $detail = Visa_detail::where('visa_id',$visa['id'])->first();
                // $media = Media::where('visa_id',$visa['id'])->first();
                // $imgs = explode(",", $media['media']);
                // $mediaArray = [];
                // foreach ($imgs as $med) {
                //     $media = url('/upload_documents',$med);
                //     $medi['media'] = $media;
                //     $mediaArray[] = $medi;
                // }
                if($visa){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'detail fetched successfully', 
                        'detail' => $visa,
                        'user_detail' => $detail,
                        // 'documents_detail' => $mediaArray,
                    ]);
                }
            }
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function visaStatus(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if($existingUser){
            $validator = Validator::make($request->all(), [
                'visa_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }else{
            $visa = Visa::where(['id'=>$request->visa_id,'user_id'=>$existingUser->id])->first();
                if($visa){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'detail fetched successfully', 
                        'visa_status' => $visa->visa_status
                    ]);
                }
            }
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function visaPayment(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        if($existingUser){
            $input = $request->all();
            $newApp =  new \App\Models\Payment;
            $newApp->user_id = $existingUser->id;
            $newApp->pay_for = $request->pay_for;
            $newApp->pay_for_id = $request->pay_for_id;
            $newApp->transaction_id = $request->transaction_id;
            $newApp->payment_status = $request->payment_status;
            $newApp->save();
            return response()->json(['status'=>'1','message'=>'Payment successfully done'], $this->successStatus);
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }


    public function store(CreateVisaAPIRequest $request)
    {
        $input = $request->all();

        $visa = $this->visaRepository->create($input);

        return $this->sendResponse($visa->toArray(), 'Visa saved successfully');
    }

    /**
     * Display the specified Visa.
     * GET|HEAD /visas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Visa $visa */
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            return $this->sendError('Visa not found');
        }

        return $this->sendResponse($visa->toArray(), 'Visa retrieved successfully');
    }

    /**
     * Update the specified Visa in storage.
     * PUT/PATCH /visas/{id}
     *
     * @param int $id
     * @param UpdateVisaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Visa $visa */
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            return $this->sendError('Visa not found');
        }

        $visa = $this->visaRepository->update($input, $id);

        return $this->sendResponse($visa->toArray(), 'Visa updated successfully');
    }

    /**
     * Remove the specified Visa from storage.
     * DELETE /visas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Visa $visa */
        $visa = $this->visaRepository->find($id);

        if (empty($visa)) {
            return $this->sendError('Visa not found');
        }

        $visa->delete();

        return $this->sendSuccess('Visa deleted successfully');
    }
}
