<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateQueryAPIRequest;
use App\Http\Requests\API\UpdateQueryAPIRequest;
use App\Models\Query;
use App\Repositories\QueryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class QueryController
 * @package App\Http\Controllers\API
 */

class QueryAPIController extends AppBaseController
{
    /** @var  QueryRepository */
    private $queryRepository;

    public function __construct(QueryRepository $queryRepo)
    {
        $this->queryRepository = $queryRepo;
    }

    /**
     * Display a listing of the Query.
     * GET|HEAD /queries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $queries = $this->queryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($queries->toArray(), 'Queries retrieved successfully');
    }

    /**
     * Store a newly created Query in storage.
     * POST /queries
     *
     * @param CreateQueryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateQueryAPIRequest $request)
    {
        $input = $request->all();

        $query = $this->queryRepository->create($input);

        return $this->sendResponse($query->toArray(), 'Query saved successfully');
    }

    /**
     * Display the specified Query.
     * GET|HEAD /queries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Query $query */
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            return $this->sendError('Query not found');
        }

        return $this->sendResponse($query->toArray(), 'Query retrieved successfully');
    }

    /**
     * Update the specified Query in storage.
     * PUT/PATCH /queries/{id}
     *
     * @param int $id
     * @param UpdateQueryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQueryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Query $query */
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            return $this->sendError('Query not found');
        }

        $query = $this->queryRepository->update($input, $id);

        return $this->sendResponse($query->toArray(), 'Query updated successfully');
    }

    /**
     * Remove the specified Query from storage.
     * DELETE /queries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Query $query */
        $query = $this->queryRepository->find($id);

        if (empty($query)) {
            return $this->sendError('Query not found');
        }

        $query->delete();

        return $this->sendSuccess('Query deleted successfully');
    }
}
