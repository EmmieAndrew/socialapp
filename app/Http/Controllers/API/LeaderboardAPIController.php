<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLeaderboardAPIRequest;
use App\Http\Requests\API\UpdateLeaderboardAPIRequest;
use App\Models\Leaderboard;
use App\Repositories\LeaderboardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LeaderboardController
 * @package App\Http\Controllers\API
 */

class LeaderboardAPIController extends AppBaseController
{
    /** @var  LeaderboardRepository */
    private $leaderboardRepository;

    public function __construct(LeaderboardRepository $leaderboardRepo)
    {
        $this->leaderboardRepository = $leaderboardRepo;
    }

    /**
     * Display a listing of the Leaderboard.
     * GET|HEAD /leaderboards
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $leaderboards = $this->leaderboardRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($leaderboards->toArray(), 'Leaderboards retrieved successfully');
    }

    /**
     * Store a newly created Leaderboard in storage.
     * POST /leaderboards
     *
     * @param CreateLeaderboardAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLeaderboardAPIRequest $request)
    {
        $input = $request->all();

        $leaderboard = $this->leaderboardRepository->create($input);

        return $this->sendResponse($leaderboard->toArray(), 'Leaderboard saved successfully');
    }

    /**
     * Display the specified Leaderboard.
     * GET|HEAD /leaderboards/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Leaderboard $leaderboard */
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            return $this->sendError('Leaderboard not found');
        }

        return $this->sendResponse($leaderboard->toArray(), 'Leaderboard retrieved successfully');
    }

    /**
     * Update the specified Leaderboard in storage.
     * PUT/PATCH /leaderboards/{id}
     *
     * @param int $id
     * @param UpdateLeaderboardAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLeaderboardAPIRequest $request)
    {
        $input = $request->all();

        /** @var Leaderboard $leaderboard */
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            return $this->sendError('Leaderboard not found');
        }

        $leaderboard = $this->leaderboardRepository->update($input, $id);

        return $this->sendResponse($leaderboard->toArray(), 'Leaderboard updated successfully');
    }

    /**
     * Remove the specified Leaderboard from storage.
     * DELETE /leaderboards/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Leaderboard $leaderboard */
        $leaderboard = $this->leaderboardRepository->find($id);

        if (empty($leaderboard)) {
            return $this->sendError('Leaderboard not found');
        }

        $leaderboard->delete();

        return $this->sendSuccess('Leaderboard deleted successfully');
    }
}
