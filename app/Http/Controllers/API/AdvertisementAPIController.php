<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdvertisementAPIRequest;
use App\Http\Requests\API\UpdateAdvertisementAPIRequest;
use App\Models\Advertisement;
use App\Repositories\AdvertisementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AdvertisementController
 * @package App\Http\Controllers\API
 */

class AdvertisementAPIController extends AppBaseController
{
    /** @var  AdvertisementRepository */
    private $advertisementRepository;

    public function __construct(AdvertisementRepository $advertisementRepo)
    {
        $this->advertisementRepository = $advertisementRepo;
    }

    /**
     * Display a listing of the Advertisement.
     * GET|HEAD /advertisements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $advertisements = $this->advertisementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($advertisements->toArray(), 'Advertisements retrieved successfully');
    }

    /**
     * Store a newly created Advertisement in storage.
     * POST /advertisements
     *
     * @param CreateAdvertisementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAdvertisementAPIRequest $request)
    {
        $input = $request->all();

        $advertisement = $this->advertisementRepository->create($input);

        return $this->sendResponse($advertisement->toArray(), 'Advertisement saved successfully');
    }

    /**
     * Display the specified Advertisement.
     * GET|HEAD /advertisements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        return $this->sendResponse($advertisement->toArray(), 'Advertisement retrieved successfully');
    }

    /**
     * Update the specified Advertisement in storage.
     * PUT/PATCH /advertisements/{id}
     *
     * @param int $id
     * @param UpdateAdvertisementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdvertisementAPIRequest $request)
    {
        $input = $request->all();

        /** @var Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement = $this->advertisementRepository->update($input, $id);

        return $this->sendResponse($advertisement->toArray(), 'Advertisement updated successfully');
    }

    /**
     * Remove the specified Advertisement from storage.
     * DELETE /advertisements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement->delete();

        return $this->sendSuccess('Advertisement deleted successfully');
    }
}
