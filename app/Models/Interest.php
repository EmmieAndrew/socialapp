<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Categories
 * @package App\Models
 * @version July 3, 2021, 8:50 am UTC
 *
 * @property string $interset_name
 * @property string $image
 * @property string $status
 */
class Interest extends Model
{
    use SoftDeletes;

    public $table = 'intersets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'interset_name',
        'image',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'interset_name' => 'string',
        'image' => 'string',
        // 'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'interset_name' => 'required|string|max:100',
        'image' => 'required|string|max:100',
        // 'status' => 'required|string',
        // 'created_at' => 'required',
        // 'updated_at' => 'nullable',
        // 'deleted_at' => 'nullable'
    ];

    
}
