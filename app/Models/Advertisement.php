<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Advertisement
 * @package App\Models
 * @version January 5, 2021, 5:00 pm UTC
 *
 * @property string $page_id
 * @property integer $post_desc
 * @property integer $media
 * @property string $tagged_users
 * @property string $status
 * @property string $type
 * @property string $page_title
 */
class Advertisement extends Model
{
    use SoftDeletes;

    public $table = 'advertisements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'page_id',
        'post_desc',
        'media',
        'tagged_users',
        'status',
        'type',
        'page_title',
        'period'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'page_id' => 'string',
        'post_desc' => 'string',
        'media' => 'string',
        'period' => 'integer',
        // 'status' => 'string',
        // 'type' => 'string',
        'page_title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'page_id' => 'nullable|integer',
        // 'post_desc' => 'nullable|integer',
        'media' => 'required',
        'period' => 'required',
        // 'updated_at' => 'nullable',
        // 'deleted_at' => 'nullable',
        // 'tagged_users' => 'nullable|string|max:100',
        // 'status' => 'nullable|string',
        // 'type' => 'required|string',
        'page_title' => 'nullable|string|max:255'
    ];

    
}
