<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pages
 * @package App\Models
 * @version December 1, 2020, 4:42 pm UTC
 *
 * @property string $page_title
 * @property string $languages
 * @property string $category
 * @property string $cover_photo
 * @property integer $user_id
 * @property string $status
 * @property integer $followers_count
 */
class Pages extends Model
{
    use SoftDeletes;

    public $table = 'pages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'page_title',
        'languages',
        'interests',
        'cover_photo',
        'user_id',
        'status',
        'followers_count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'page_title' => 'string',
        'languages' => 'string',
        'interests' => 'string',
        'cover_photo' => 'string',
        'user_id' => 'integer',
        'status' => 'string',
        'followers_count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    //     'page_title' => 'nullable|string|max:100',
    //     'languages' => 'nullable|string|max:100',
    //     'interests' => 'nullable|string|max:100',
    //     'cover_photo' => 'nullable|string|max:255',
    //     'user_id' => 'nullable|integer',
    //     'status' => 'required|string',
    //     'followers_count' => 'required|integer',
    //     'created_at' => 'required',
    //     'updated_at' => 'nullable',
    //     'deleted_at' => 'nullable'
    ];

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function getInerestPage()
    {
        return $this->facility->pluck('interests')->implode(',');
    }
    
}
