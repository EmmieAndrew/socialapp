<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Banner
 * @package App\Models
 * @version November 30, 2020, 2:35 pm UTC
 *
 * @property string $type
 * @property string $title
 * @property string $image
 */
class Banner extends Model
{
    use SoftDeletes;

    public $table = 'notifications';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        // 'type',
        // 'title',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        // 'type' => 'string',
        // 'title' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'type' => 'nullable|string|max:100',
        // 'title' => 'nullable|string|max:100',
        'image' => 'required',
        // 'created_at' => 'required',
        // 'updated_at' => 'required',
        // 'deleted_at' => 'required'
    ];

    
}
