<?php

namespace App\Models;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
	use SoftDeletes;

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    protected $table = 'action_logs';

    // function user(){
    //     return $this->belongsTo('App\Models\User');
    // }

}
