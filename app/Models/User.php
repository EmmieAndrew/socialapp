<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version April 25, 2020, 7:48 pm UTC
 *
 * @property string $device_id
 * @property string $device_type
 * @property string $remember_token
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $DOB
 * @property string $country
 * @property string $gender
 * @property string $phone_no
 * @property string $image
 * @property string $type
 * @property string $login_status
 * @property string|\Carbon\Carbon $email_verified_at
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'device_id',
        'device_type',
        'remember_token',
        'name',
        'email',
        'password',
        'DOB',
        'country',
        'gender',
        'phone_no',
        'image',
        'type',
        'login_status',
        'email_verified_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'device_id' => 'string',
        'device_type' => 'string',
        'remember_token' => 'string',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'DOB' => 'string',
        'country' => 'string',
        'gender' => 'string',
        'phone_no' => 'string',
        'image' => 'string',
        'type' => 'string',
        'login_status' => 'string',
        'email_verified_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        // 'password' => 'required',
        // 'DOB' => 'required',
        // 'country' => 'required',
        // 'gender' => 'required',
        // 'phone_no' => 'required',
        // 'image' => 'required',
        // 'type' => 'required',
        // 'login_status' => 'required'
    ];

    public function pages(){
        return $this->hasMany('App\Models\Pages');
    }

    // public function visa_detail(){
    //     return $this->hasOne('App\Models\Visa_detail');
    // }

    // public function visa_detail(){
    //     return $this->hasOne('App\Models\Visa_detail');
    // }
    
}
