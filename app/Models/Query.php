<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Query
 * @package App\Models
 * @version December 1, 2020, 4:27 pm UTC
 *
 * @property integer $user_id
 * @property string $subject
 * @property string $content
 * @property string $email
 */
class Query extends Model
{
    use SoftDeletes;

    public $table = 'queries';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'subject',
        'content',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'subject' => 'string',
        'content' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable|integer',
        'subject' => 'nullable|string|max:255',
        'content' => 'nullable|string|max:255',
        'created_at' => 'required',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'email' => 'nullable|string|max:100'
    ];

    
}
