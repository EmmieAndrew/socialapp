<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Language
 * @package App\Models
 * @version December 3, 2020, 6:34 pm UTC
 *
 * @property string $language
 */
class Language extends Model
{
    use SoftDeletes;

    public $table = 'languages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'language'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'language' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'language' => 'required|string|max:100',
        // 'created_at' => 'required',
        // 'updated_at' => 'nullable',
        // 'deleted_at' => 'nullable'
    ];

    
}
