<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Visa
 * @package App\Models
 * @version November 8, 2020, 3:40 am UTC
 *
 * @property string $type
 * @property string $country
 * @property string $upload_doc
 * @property string $passport_doc
 * @property string $photo
 * @property string $familty_paper
 * @property integer $cost
 * @property integer $due_cost
 * @property string $applied_date
 * @property string $payment_type
 * @property string $payment_status
 * @property string $multi_entry_doc_submission
 * @property string $country_code
 */
class Visa_detail extends Model
{
    use SoftDeletes;

    public $table = 'visa_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'name',
        'DOB',
        'passport_no',
        'flight_booked',
        'accomodation_booked',
        'passanger_count',
        'kids'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_id' => 'integer',
        'name' => 'string',
        'DOB' => 'string',
        'passport_no' => 'string',
        'flight_booked' => 'string',
        'accomodation_booked' => 'string',
        'kids' => 'string',
        'passanger_count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_at' => 'required'
    ];

    
}
