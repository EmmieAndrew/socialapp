<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version April 25, 2020, 7:48 pm UTC
 *
 * @property string $device_id
 * @property string $device_type
 * @property string $remember_token
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $DOB
 * @property string $country
 * @property string $gender
 * @property string $phone_no
 * @property string $image
 * @property string $type
 * @property string $login_status
 * @property string|\Carbon\Carbon $email_verified_at
 */
class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pay_for',
        'pay_for_id',
        'user_id',
        'transaction_id',
        'payment_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pay_for' => 'string',
        'pay_for_id' => 'string',
        'user_id' => 'string',
        'transaction_id' => 'string',
        'payment_status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'post_id' => 'required',
        // 'user_id' => 'required',
        // 'media' => 'required'
    ];

    
}
