<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 * @package App\Models
 * @version December 1, 2020, 4:33 pm UTC
 *
 * @property integer $report_by_id
 * @property integer $entity_id
 * @property string $report_type
 * @property string $report_reason
 */
class Report extends Model
{
    use SoftDeletes;

    public $table = 'reports';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'report_by_id',
        'entity_id',
        'report_type',
        'report_reason'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'report_by_id' => 'integer',
        'entity_id' => 'integer',
        'report_type' => 'string',
        'report_reason' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'report_by_id' => 'required|integer',
        'entity_id' => 'required|integer',
        'report_type' => 'required|string',
        'created_at' => 'required',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'report_reason' => 'nullable|string|max:255'
    ];

    
}
