<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Leaderboard
 * @package App\Models
 * @version April 15, 2021, 6:33 pm UTC
 *
 * @property string|\Carbon\Carbon $start_day
 * @property string|\Carbon\Carbon $end_day
 * @property string $notes
 * @property string $status
 */
class Leaderboard extends Model
{
    use SoftDeletes;

    public $table = 'leaderboard';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'start_day',
        'end_day',
        'notes',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'start_day' => 'datetime',
        'end_day' => 'datetime',
        'notes' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'start_day' => 'required',
        'end_day' => 'required',
        'notes' => 'required|string',
        // 'status' => 'required|string',
        // 'created_at' => 'required',
        // 'updated_at' => 'nullable',
        // 'deleted_at' => 'nullable'
    ];

    
}
