<?php

namespace App\Repositories;

use App\Models\Query;
use App\Repositories\BaseRepository;

/**
 * Class QueryRepository
 * @package App\Repositories
 * @version December 1, 2020, 4:27 pm UTC
*/

class QueryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'subject',
        'content',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Query::class;
    }
}
