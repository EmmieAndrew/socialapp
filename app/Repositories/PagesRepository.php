<?php

namespace App\Repositories;

use App\Models\Pages;
use App\Repositories\BaseRepository;

/**
 * Class PagesRepository
 * @package App\Repositories
 * @version December 1, 2020, 4:42 pm UTC
*/

class PagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'page_title',
        'languages',
        'category',
        'cover_photo',
        'user_id',
        'status',
        'followers_count'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pages::class;
    }
}
