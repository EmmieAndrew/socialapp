<?php

namespace App\Repositories;

use App\Models\Advertisement;
use App\Repositories\BaseRepository;

/**
 * Class AdvertisementRepository
 * @package App\Repositories
 * @version January 5, 2021, 5:00 pm UTC
*/

class AdvertisementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'page_id',
        'post_desc',
        'media',
        'tagged_users',
        'status',
        'type',
        'page_title'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Advertisement::class;
    }
}
