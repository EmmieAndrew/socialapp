<?php

namespace App\Repositories;

use App\Models\Interest;
use App\Repositories\BaseRepository;

/**
 * Class InterestRepository
 * @package App\Repositories
 * @version April 15, 2021, 6:32 pm UTC
*/

class InterestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'start_day',
        'end_day',
        'notes',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Interest::class;
    }
}
