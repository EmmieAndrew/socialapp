<?php

namespace App\Repositories;

use App\Models\Visa;
use App\Repositories\BaseRepository;

/**
 * Class VisaRepository
 * @package App\Repositories
 * @version November 8, 2020, 3:40 am UTC
*/

class VisaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'country',
        'upload_doc',
        'passport_doc',
        'photo',
        'familty_paper',
        'cost',
        'due_cost',
        'applied_date',
        'payment_type',
        'payment_status',
        'multi_entry_doc_submission',
        'country_code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Visa::class;
    }
}
