<?php

namespace App\Repositories;

use App\Models\Leaderboard;
use App\Repositories\BaseRepository;

/**
 * Class LeaderboardRepository
 * @package App\Repositories
 * @version April 15, 2021, 6:33 pm UTC
*/

class LeaderboardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'start_day',
        'end_day',
        'notes',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Leaderboard::class;
    }
}
