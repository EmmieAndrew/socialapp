<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Visa;
use Faker\Generator as Faker;

$factory->define(Visa::class, function (Faker $faker) {

    return [
        'type' => $faker->word,
        'country' => $faker->word,
        'upload_doc' => $faker->word,
        'passport_doc' => $faker->word,
        'photo' => $faker->word,
        'familty_paper' => $faker->word,
        'cost' => $faker->randomDigitNotNull,
        'due_cost' => $faker->randomDigitNotNull,
        'applied_date' => $faker->word,
        'payment_type' => $faker->word,
        'payment_status' => $faker->word,
        'multi_entry_doc_submission' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'country_code' => $faker->word
    ];
});
