<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Advertisement;
use Faker\Generator as Faker;

$factory->define(Advertisement::class, function (Faker $faker) {

    return [
        'page_id' => $faker->word,
        'post_desc' => $faker->randomDigitNotNull,
        'media' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'tagged_users' => $faker->word,
        'status' => $faker->word,
        'type' => $faker->word,
        'page_title' => $faker->word
    ];
});
