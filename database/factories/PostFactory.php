<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    return [
        'page_id' => $faker->randomDigitNotNull,
        'post_desc' => $faker->word,
        'media' => $faker->word,
        'mediatype' => $faker->word,
        'tagged_users' => $faker->word,
        'tags' => $faker->word,
        'status' => $faker->word,
        'type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
