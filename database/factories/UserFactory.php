<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'device_id' => $faker->word,
        'device_type' => $faker->word,
        'remember_token' => $faker->word,
        'name' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'DOB' => $faker->word,
        'country' => $faker->word,
        'gender' => $faker->word,
        'phone_no' => $faker->word,
        'image' => $faker->word,
        'type' => $faker->word,
        'login_status' => $faker->word,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->word
    ];
});
