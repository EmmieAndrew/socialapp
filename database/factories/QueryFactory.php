<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Query;
use Faker\Generator as Faker;

$factory->define(Query::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'subject' => $faker->word,
        'content' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'email' => $faker->word
    ];
});
