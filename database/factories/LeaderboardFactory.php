<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Leaderboard;
use Faker\Generator as Faker;

$factory->define(Leaderboard::class, function (Faker $faker) {

    return [
        'start_day' => $faker->date('Y-m-d H:i:s'),
        'end_day' => $faker->date('Y-m-d H:i:s'),
        'notes' => $faker->text,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
