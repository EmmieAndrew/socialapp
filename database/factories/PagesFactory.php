<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pages;
use Faker\Generator as Faker;

$factory->define(Pages::class, function (Faker $faker) {

    return [
        'page_title' => $faker->word,
        'languages' => $faker->word,
        'category' => $faker->word,
        'cover_photo' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'followers_count' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
