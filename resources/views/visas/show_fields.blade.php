<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <style>
         h3 {
         margin-top: 5%;
         padding-left: 24px;
         }
         .user_info {
         display: -webkit-box;
         }
         .activate_button {
         float: right;
         }
         h5 {
         margin-bottom: 5px;
         }
         .icon {
         font-size: xx-large;
         padding-right: 15px;
         }
      </style>
   </head>
   <body>
    <div class="container">
        <div class="row">

                <h3>
                    @if($visa->type == 'SE')
                    <center><b>Single Entry</b></center>
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @else
                    <center><b><i>Multi Entry</i></b></center>
                    <i class="fas fa-angle-right"></i>
                    @endif
                </h3>
                
            <div class="col-sm-6">
                <div class="user_info">
                  <div class="user_image">
                    <a href="http://localhost/marketplace/public/upload_documents/<?= $visa->upload_doc; ?>" target="_blank"><img src="{!! url('/upload_documents/'. $visa->upload_doc) !!}" width="auto;" height="150"></a>
                </div>
                <div class="user_image">
                    <a href="http://localhost/marketplace/public/upload_documents/<?= $visa->passport_doc; ?>" target="_blank"><img src="{!! url('/upload_documents/'. $visa->passport_doc) !!}" width="auto;" height="150"></a>
                </div>
                <div class="user_image">
                    <a href="http://localhost/marketplace/public/upload_documents/<?= $visa->familty_paper; ?>" target="_blank"><img src="{!! url('/upload_documents/'. $visa->familty_paper) !!}" width="auto;" height="150"></a>
                </div>
                <!-- <div class="user_image">
                    <a href="http://localhost/marketplace/public/visa/<?= $visa->photo; ?>" target="_blank"><img src="{!! url('/visa/'. $visa->photo) !!}" width="auto;" height="200"></a>
                </div> -->
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="row">
                <table>
                    <tr>
                        <td>
                            <div class="icon">
                                <i class="fa fa-user-circle"></i>
                            </div>
                        </td>
                        <td><h5>Customer Name:</h5><i>{!! $visa->visa_detail['name'] !!}</i></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fas fa-envelope-open"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Passport:
                       </h5>
                       {!! $visa->passport_doc !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>

        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fa fa-calendar"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Date of Birth:
                       </h5>
                       {!! $visa->visa_detail['DOB'] !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>
     </div>

     <br>

     <div class="row">
        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fa fa-globe"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Country:
                       </h5>
                       {!! $visa->country !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>

        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fa fa-phone"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Phone Number:
                       </h5>
                       {!! $visa->visa_detail['phone_no'] !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>
        
        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fa fa-calendar-check-o"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Created At:
                       </h5>
                       {!! $visa->created_at !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>
     </div>

     <br>

     <div class="row">
        <div class="col-sm-4">
           <div class="row">
              <table>
                 <tr>
                    <td>
                       <div class="icon">
                          <i class="fa fa-calendar-plus-o"></i>
                       </div>
                    </td>
                    <td>
                       <h5>Updated At:
                       </h5>
                       {!! $visa->updated_at !!}
                    </td>
                 </tr>
              </table>
           </div>
        </div>
    </div>
</div> 
</body>
</html>
