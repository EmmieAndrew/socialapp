<div class="table-responsive-sm">
    <table class="table table-striped" id="visas-table">
        <thead>
            <tr>
                <th scope="col">Visa ID</th>
                <th>Name</th>
                <th>Visa Type</th>
                <th>Country</th>
                <th>Passport</th>
                <th>Upload Document</th>
                <th>Family Paper</th>
                <th>Cost</th>
                <th>Applied Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $i = ($visas->currentPage() - 1) * $visas->perPage() + 1; ?>
        @foreach($visas as $visa)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{{ $visa->visa_detail['name'] }}</td>
                <td>@if($visa->type =='SE')         
                Single Entry        
                @else
                Multi Entry       
                @endif
                </td>
                <td>{{ $visa->country }}</td>
                <td><a href="http://localhost/marketplace/public/upload_documents/<?= $visa->passport_doc; ?>" target="_blank"><img src="{!! url('/upload_documents/'. $visa->passport_doc) !!}" width="72" height="76"></a></td>
                <td><a href="http://localhost/marketplace/public/upload_documents/<?= $visa->upload_doc; ?>" target="_blank"><img src="{!! url('/upload_documents/'. $visa->upload_doc) !!}" width="72" height="76"></a></td>
                <td><a href="http://localhost/marketplace/public/upload_documents/<?= $visa->familty_paper; ?>" target="_blank">{{$visa->familty_paper}}</a></td>
                <td>{{ $visa->cost }}</td>
                <td>{{ $visa->applied_date->format('Y-m-d') }}</td>
                <td>
                    {!! Form::open(['route' => ['visas.destroy', $visa->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('visas.show', [$visa->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('visas.edit', [$visa->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        <a href="{{ url('/notify/' . $visa->id) }}" class='btn btn-ghost-danger'><i class="fa fa-bell"></i></a>
                        <!-- <a href="{{ url('/ban_unban/' . $visa->id) }}" class='btn btn-ghost-info'><i class="fa fa-ban"></i></a> -->
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="col-5">
            <div class="float-right">
                {!! $visas->render() !!}
            </div>
        </div>
</div>