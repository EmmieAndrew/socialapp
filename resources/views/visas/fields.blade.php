<!-- Type Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Country Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_code', 'Country Code:') !!}
    {!! Form::text('country_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<!-- Upload Doc Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('upload_doc', 'Upload Doc:') !!}
    {!! Form::text('upload_doc', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Passport Doc Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('passport_doc', 'Passport Doc:') !!}
    {!! Form::text('passport_doc', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Photo Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Familty Paper Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('familty_paper', 'Familty Paper:') !!}
    {!! Form::text('familty_paper', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::number('cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Due Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('due_cost', 'Due Cost:') !!}
    {!! Form::number('due_cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Applied Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('applied_date', 'Applied Date:') !!}
    {!! Form::text('applied_date', null, ['class' => 'form-control','id'=>'applied_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#applied_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Payment Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_type', 'Payment Type:') !!}
    {!! Form::text('payment_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    {!! Form::text('payment_status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Multi Entry Doc Submission Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('multi_entry_doc_submission', 'Multi Entry Doc Submission:') !!}
    {!! Form::text('multi_entry_doc_submission', null, ['class' => 'form-control']) !!}
</div> -->


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('visas.index') }}" class="btn btn-secondary">Cancel</a>
</div>
