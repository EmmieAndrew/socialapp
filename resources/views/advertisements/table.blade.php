<div class="table-responsive-sm">
    <table class="table table-striped" id="advertisements-table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th>Page</th>
                <th>Page Title</th>
                <th>Post Desc</th>
                <th>Media</th>
                <th>Period</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
             <?php $i = ($advertisements->currentPage() - 1) * $advertisements->perPage() + 1; ?>
        @foreach($advertisements as $advertisement)
        <?php
        $pageName = \App\Models\Pages::where('id',$advertisement->page_id)->first();
        ?>
            <tr>
                <td scope="row">{!! $i++ !!}</td> 
                <td>   
                    @if($advertisement->page_id)
                    <b>{{ ucfirst($pageName->page_title) }}</b>
                    @else
                    <b><p class="text-info">Sponsored Advertisement</p></b>
                    @endif
                </td>
                <td>@if($advertisement->page_title)
                    {{ $advertisement->page_title }}
                    @else
                    No Data
                    @endif
                </td>
                <td>{{ $advertisement->post_desc }}</td>
                <td><a href="{!! url('/advertisement/'. $advertisement->media) !!}" target="_blank">
                    <img src="{!! url('/advertisement/'. $advertisement->media) !!}" width="auto;" height="80"></a>
                </td>
                <td>{{ $advertisement->period }} Days</td>
                <td>
                    {!! Form::open(['route' => ['advertisements.destroy', $advertisement->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('advertisements.show', [$advertisement->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a> -->
                       <!--  <a href="{{ route('advertisements.edit', [$advertisement->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> -->
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
            <div class="float-right">
                {!! $advertisements->render() !!}
            </div>
        </div>
</div>