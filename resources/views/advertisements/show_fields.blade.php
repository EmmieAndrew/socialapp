<!-- Page Id Field -->
<div class="form-group">
    {!! Form::label('page_id', 'Page Id:') !!}
    <p>{{ $advertisement->page_id }}</p>
</div>

<!-- Post Desc Field -->
<div class="form-group">
    {!! Form::label('post_desc', 'Post Desc:') !!}
    <p>{{ $advertisement->post_desc }}</p>
</div>

<!-- Media Field -->
<div class="form-group">
    {!! Form::label('media', 'Media:') !!}
    <p>{{ $advertisement->media }}</p>
</div>

<!-- Tagged Users Field -->
<!-- <div class="form-group">
    {!! Form::label('tagged_users', 'Tagged Users:') !!}
    <p>{{ $advertisement->tagged_users }}</p>
</div>
 -->
<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $advertisement->status }}</p>
</div> -->

<!-- Type Field -->
<!-- <div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $advertisement->type }}</p>
</div> -->

<!-- Page Title Field -->
<div class="form-group">
    {!! Form::label('page_title', 'Page Title:') !!}
    <p>{{ $advertisement->page_title }}</p>
</div>

