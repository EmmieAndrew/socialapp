<!-- Page Id Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('page_id', 'Page Id:') !!}
    {!! Form::text('page_id', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div> -->

<div class="form-group col-sm-6">
    <label for="title">Choose Page:</label>
    <select id="page" name="page" class="form-control">
        <option value="" selected disabled>Select</option>
        @foreach($pages as $key => $page)
        <option value="{{$key}}"> {{$page}}</option>
        @endforeach
    </select>
</div>

<!-- Post Desc Field -->


<!-- <div class="form-group col-sm-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Post Description: <span> As Manager *</span></label>
            <div class="col-xs-12">
                {!! Form::text('post_desc', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div> -->




<div class="form-group col-sm-6">
    <label for="inputName" class="col-xs-12 control-label">Post Description: </label>
    {!! Form::text('post_desc', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('media', 'Post Media:') !!}
    <input type="file" class="form-control" name="media"/>
</div>
<!-- Media Field -->
<!-- Page Title Field -->
<div class="form-group col-sm-6">
    <label for="inputName" class="col-xs-12 control-label">Title (For Outside Pages): </label>
    {!! Form::text('page_title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<div class="form-group col-sm-6">
    <label for="inputName" class="col-xs-12 control-label">Validity (In Days): </label>
    {!! Form::number('period', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('advertisements.index') }}" class="btn btn-secondary">Cancel</a>
</div>
