@extends('layouts.app')
@section('content')
<style>
  .lightbluebg {
    background: #a4b6c5;
    color: #FFF;
    font-family: Lato, sans-serif;
    margin: 10px 0;
    padding-top: 2px;
    border-radius: 5px;
    padding-left: 10px;
}
.lightbluebg1 {
    background: #02b39a;
    color: #FFF;
    font-family: Lato, sans-serif;
    margin: 10px 0;
    padding-top: 2px;
    border-radius: 5px;
    padding-left: 10px;
}
.lightbluebg2 {
    background: #5eacda;
    color: #FFF;
    font-family: Lato, sans-serif;
    margin: 10px 0;
    padding-top: 2px;
    border-radius: 5px;
    padding-left: 10px;
}
.lightbluebg3 {
    background: #00c0ef;
    color: #FFF;
    font-family: Lato, sans-serif;
    margin: 10px 0;
    padding-top: 2px;
    border-radius: 5px;
    padding-left: 10px;
}
.box.bg-white {
    background: #ecf8fb;
    border-radius: 5px;
}
</style>
<?php
use App\Models\User;
?>
<section class="content-header">
   <h1><center>Welcome to Social Dashboard</center></h1>
</section>
<div class="container-fluid">

   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="lightbluebg">
      <div class="white-box">
         <h3 class="box-title"><i class="ti-user text-info"></i> Registered Users</h3>
         <div class="text-left">
            <span class="text-muted"></span>
            <?php $user = User::count();?>
            <h1>
              <sup>
                <i class="fa fa-user-plus" aria-hidden="true"></i>
              </sup>
            </h1>
         </div>
      </div>
    </div>
   </div>

   <div class="row row-md mb-2">
      <div class="col-md-12">
         <div class="box bg-white">
            <div class="box-block clearfix">
               <h5 class="float-xs-left"><center><b>Recent Users</b></center></h5>
               <div class="float-xs-right">
                  <button class="btn btn-link btn-sm text-muted" type="button"><i class="ti-close"></i></button>
               </div>
            </div>
            <?php $users = User::orderBy('id', 'desc')->take(8)->get(); ?>
            <table class="table mb-md-0">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Phone Number</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($users as $val)
                <tr>
                    <td><u>#{{ $val->id}}</u></td>
                    <td>{{ ucfirst($val->name) }}</td>
                    <td>{{ $val->email}}</td>
                    <td>{{ $val->phone_no}}</td>
                </tr>
            @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection