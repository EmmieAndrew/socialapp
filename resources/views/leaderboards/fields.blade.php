<!-- Start Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_day', 'Start Day:') !!}
    {!! Form::text('start_day', null, ['class' => 'form-control','id'=>'start_day']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#start_day').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- End Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_day', 'End Day:') !!}
    {!! Form::text('end_day', null, ['class' => 'form-control','id'=>'end_day']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#end_day').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notes', 'Notes:') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('leaderboards.index') }}" class="btn btn-secondary">Cancel</a>
</div>
