<!-- Start Day Field -->
<div class="form-group">
    {!! Form::label('start_day', 'Start Day:') !!}
    <p>{{ $leaderboard->start_day }}</p>
</div>

<!-- End Day Field -->
<div class="form-group">
    {!! Form::label('end_day', 'End Day:') !!}
    <p>{{ $leaderboard->end_day }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $leaderboard->notes }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $leaderboard->status }}</p>
</div>

