<div class="table-responsive-sm">
    <table class="table table-striped" id="leaderboards-table">
        <thead>
            <tr>
                <th>Start Day</th>
        <th>End Day</th>
        <th>Notes</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($leaderboards as $leaderboard)
            <tr>
                <td>{{ $leaderboard->start_day }}</td>
            <td>{{ $leaderboard->end_day }}</td>
            <td>{{ $leaderboard->notes }}</td>
            <td>{{ $leaderboard->status }}</td>
                <td>
                    {!! Form::open(['route' => ['leaderboards.destroy', $leaderboard->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('leaderboards.show', [$leaderboard->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('leaderboards.edit', [$leaderboard->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>