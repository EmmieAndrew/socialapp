<div class="table-responsive-sm">
    <table class="table table-striped" id="reports-table">
        <thead>
            <tr>
                <th>Report By Id</th>
        <th>Entity Id</th>
        <th>Report Type</th>
        <th>Report Reason</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <td>{{ $report->report_by_id }}</td>
            <td>{{ $report->entity_id }}</td>
            <td>{{ $report->report_type }}</td>
            <td>{{ $report->report_reason }}</td>
                <td>
                    {!! Form::open(['route' => ['reports.destroy', $report->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reports.show', [$report->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('reports.edit', [$report->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>