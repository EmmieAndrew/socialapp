<!-- Report By Id Field -->
<div class="form-group">
    {!! Form::label('report_by_id', 'Report By Id:') !!}
    <p>{{ $report->report_by_id }}</p>
</div>

<!-- Entity Id Field -->
<div class="form-group">
    {!! Form::label('entity_id', 'Entity Id:') !!}
    <p>{{ $report->entity_id }}</p>
</div>

<!-- Report Type Field -->
<div class="form-group">
    {!! Form::label('report_type', 'Report Type:') !!}
    <p>{{ $report->report_type }}</p>
</div>

<!-- Report Reason Field -->
<div class="form-group">
    {!! Form::label('report_reason', 'Report Reason:') !!}
    <p>{{ $report->report_reason }}</p>
</div>

