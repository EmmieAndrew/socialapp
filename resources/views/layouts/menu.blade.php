</li>
<li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! url('/home') !!}">
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span>Users</span>
    </a>
</li>
<!-- <li class="nav-item {{ Request::is('visas*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('visas.index') }}">
        <i class="fa fa-cc-visa"></i>
        <span> Visa Applications</span>
    </a>
</li> -->
<li class="nav-item {{ Request::is('banners*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('banners.index') }}">
        <i class="fa fa-flag" aria-hidden="true"></i>
        <span>Banners</span>
    </a>
</li>
<!-- <li class="nav-item {{ Request::is('queries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('queries.index') }}">
        <i class="fa fa-question-circle" aria-hidden="true"></i>
        <span>Queries</span>
    </a>
</li> -->
<li class="nav-item {{ Request::is('reports*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('reports.index') }}">
        <i class="fa fa-window-close-o" aria-hidden="true"></i>
        <span>Reports</span>
    </a>
</li>
<li class="nav-item {{ Request::is('pages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('pages.index') }}">
        <i class="fa fa-file" aria-hidden="true"></i>
        <span>Pages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('posts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('posts.index') }}">
        <i class="fa fa-clipboard" aria-hidden="true"></i>
        <span>Posts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="fa fa-heart" aria-hidden="true"></i>
        <span>Interests</span>
    </a>
</li>
<li class="nav-item {{ Request::is('languages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('languages.index') }}">
        <i class="fa fa-language" aria-hidden="true"></i>
        <span>Languages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('advertisements*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('advertisements.index') }}">
        <i class="fa fa-buysellads" aria-hidden="true"></i>
        <span>Advertisements</span>
    </a>
</li>
<li class="nav-item {{ Request::is('leaderboards*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('leaderboards.index') }}">
        <i class="fa fa-calendar-o"></i>
        <span>Leaderboards</span>
    </a>
</li>
<!-- <li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Categories</span>
    </a>
</li> -->
