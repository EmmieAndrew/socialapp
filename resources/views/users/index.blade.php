@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Users</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <!-- <i class="fa fa-align-justify"></i>
                             Users -->
                             <div style="text-align: end;">
                                <form action="searchuser" method="get">
                                    <input type="search" style="height: 35px;" class="search" name="search" placeholder="Search..">
                                    <button type="submit" class="btn btn-success button">Search</button>
                                </form>
                            </div>
                             <!-- <a class="pull-right" href="{{ route('users.create') }}"><i class="fa fa-plus-square fa-lg"></i></a> -->
                         </div>
                         <div class="card-body">
                             @include('users.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

