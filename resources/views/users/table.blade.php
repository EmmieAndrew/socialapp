<div class="table-responsive-sm">
    <table class="table table-striped" id="users-table">
        <thead>
            <tr>
                <th scope="col">ID</th>
        <th>Name</th>
        <th>Email</th>
        <!-- <th>Dob</th> -->
        <th>Country</th>
        <th>Gender</th>
        <th>Phone No</th>
        <!-- <th>Image</th> -->
        <th>Ban</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($users->currentPage() - 1) * $users->perPage() + 1; ?>
        @foreach($users as $user)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <!-- <td>{{ $user->DOB }}</td> -->
            <td>{{ $user->country }}</td>
            <td>{{ $user->gender }}</td>
            <td>{{ $user->phone_no }}</td>
            <!-- <td>{{ $user->image }}</td> -->
            <td>
                @if($user->ban == 1)
                <a href="{{ url('/userban/' . $user->id) }}" class='btn btn-ghost-danger'>
                   <i class="fa fa-check-square" aria-hidden="true"></i></a>
                @endif
                @if($user->ban == 0)
                <a href="{{ url('/userban/' . $user->id) }}" class='btn btn-ghost-danger'>
                <i class="fa fa-ban"></i></a>
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('users.show', [$user->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                    <a href="{{ route('users.edit', [$user->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
            <div class="float-right">
                {!! $users->render() !!}
            </div>
        </div>
</div>