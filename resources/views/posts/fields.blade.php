<!-- Page Id Field -->
<div class="form-group col-sm-6">
    <label for="title">Choose Page:</label>
    <select id="page" name="page" class="form-control">
        <option value="" selected disabled>Select</option>
        @foreach($pages as $key => $page)
        <option value="{{$key}}"> {{$page}}</option>
        @endforeach
    </select>
</div>

<!-- Post Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post_desc', 'Post Desc:') !!}
    {!! Form::text('post_desc', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Media Field -->
<div class="form-group col-sm-6">
    {!! Form::label('media', 'Post Media:') !!}
    <input type="file" class="form-control" name="media[]" multiple="" />
</div>

<!-- Mediatype Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('mediatype', 'Mediatype:') !!}
    {!! Form::text('mediatype', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div> -->

<!-- Tagged Users Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('tagged_users', 'Tagged Users:') !!}
    {!! Form::text('tagged_users', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div> -->

<!-- Tags Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('tags', 'Tags:') !!}
    {!! Form::text('tags', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div> -->

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Type Field -->

<div class="form-group col-sm-6">
    <label for="title">Post Type:</label>
    <select id="type" name="type" class="form-control">
        <option value="P" selected disabled>Select</option>
        <option>Post</option>
    </select>
</div>
<!-- <div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('posts.index') }}" class="btn btn-secondary">Cancel</a>
</div>
