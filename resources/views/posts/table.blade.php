<div class="table-responsive-sm">
    <table class="table table-striped" id="posts-table">
        <thead>
            <tr>
                <th>Page</th>
                <th>Description</th>
                <th>Media</th>
                <!-- <th>Mediatype</th> -->
                <!-- <th>Tagged Users</th> -->
                <!-- <th>Tags</th> -->
                <!-- <th>Status</th> -->
                <th>Post Type</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
        <?php
        $pageName = \App\Models\Pages::where('id',$post->page_id)->first();
        ?>
            <tr>
                <td>
                    <b>{{ ucfirst($pageName->page_title) }}</b>
                </td>
                <td>{{ $post->post_desc }}</td>
                <td>{{ $post->media }}</td>
                <!-- <td>{{ $post->mediatype }}</td> -->
                <!-- <td>{{ $post->tagged_users }}</td> -->
                <!-- <td>{{ $post->tags }}</td> -->
                <!-- <td>{{ $post->status }}</td> -->
                <td><b><p class="text-info">Image</p></b></td>
                <td>
                    {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('posts.show', [$post->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('posts.edit', [$post->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> -->
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>