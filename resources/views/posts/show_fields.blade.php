<!-- Page Id Field -->
<div class="form-group">
    {!! Form::label('page_id', 'Page Id:') !!}
    <p>{{ $post->page_id }}</p>
</div>

<!-- Post Desc Field -->
<div class="form-group">
    {!! Form::label('post_desc', 'Post Desc:') !!}
    <p>{{ $post->post_desc }}</p>
</div>

<!-- Media Field -->
<div class="form-group">
    {!! Form::label('media', 'Media:') !!}
    <p>{{ $post->media }}</p>
</div>

<!-- Mediatype Field -->
<div class="form-group">
    {!! Form::label('mediatype', 'Mediatype:') !!}
    <p>{{ $post->mediatype }}</p>
</div>

<!-- Tagged Users Field -->
<div class="form-group">
    {!! Form::label('tagged_users', 'Tagged Users:') !!}
    <p>{{ $post->tagged_users }}</p>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', 'Tags:') !!}
    <p>{{ $post->tags }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $post->status }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $post->type }}</p>
</div>

