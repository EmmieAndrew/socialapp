<div class="table-responsive-sm">
    <table class="table table-striped" id="interests-table">
        <thead>
            <tr>
                <th>Start Day</th>
        <th>End Day</th>
        <th>Notes</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($interests as $interest)
            <tr>
                <td>{{ $interest->start_day }}</td>
            <td>{{ $interest->end_day }}</td>
            <td>{{ $interest->notes }}</td>
            <td>{{ $interest->status }}</td>
                <td>
                    {!! Form::open(['route' => ['interests.destroy', $interest->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('interests.show', [$interest->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('interests.edit', [$interest->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>