<!-- Start Day Field -->
<div class="form-group">
    {!! Form::label('start_day', 'Start Day:') !!}
    <p>{{ $interest->start_day }}</p>
</div>

<!-- End Day Field -->
<div class="form-group">
    {!! Form::label('end_day', 'End Day:') !!}
    <p>{{ $interest->end_day }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $interest->notes }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $interest->status }}</p>
</div>

