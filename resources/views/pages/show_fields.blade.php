<!-- Page Title Field -->
<div class="form-group">
    {!! Form::label('page_title', 'Page Title:') !!}
    <p>{{ $pages->page_title }}</p>
</div>

<!-- Languages Field -->
<div class="form-group">
    {!! Form::label('languages', 'Languages:') !!}
    <p>{{ $pages->languages }}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('interests', 'Interests:') !!}
    <p>{{ $pages->interests }}</p>
</div>

<!-- Cover Photo Field -->
<div class="form-group">
    {!! Form::label('cover_photo', 'Cover Photo:') !!}
    <p>{{ $pages->cover_photo }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $pages->user_id }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $pages->status }}</p>
</div>

<!-- Followers Count Field -->
<div class="form-group">
    {!! Form::label('followers_count', 'Followers Count:') !!}
    <p>{{ $pages->followers_count }}</p>
</div>

