<!-- Page Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('page_title', 'Page Title:') !!}
    {!! Form::text('page_title', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Languages Field -->
<div class="form-group col-sm-6">
    {!! Form::label('languages', 'Languages:') !!}
    {!! Form::text('languages', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('interests', 'Interests:') !!}
    {!! Form::text('interests', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Cover Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('media', 'Page Media:') !!}
    <input type="file" class="form-control" name="cover_photo"/>
</div>

<!-- User Id Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div> -->

<div class="form-group col-sm-6">
    <label for="title">Choose User:</label>
    <select id="user" name="user" class="form-control">
        <option value="" selected disabled>Select</option>
        @foreach($users as $key => $user)
        <option value="{{$key}}"> {{$user}}</option>
        @endforeach
    </select>
</div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Followers Count Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('followers_count', 'Followers Count:') !!}
    {!! Form::number('followers_count', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pages.index') }}" class="btn btn-secondary">Cancel</a>
</div>
