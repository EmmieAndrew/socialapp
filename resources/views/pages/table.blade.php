<div class="table-responsive-sm">
    <table class="table table-striped" id="pages-table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th>Page Title</th>
               <!--  <th>Languages</th>
                <th>Interests</th> -->
                <th>Cover Photo</th>
                <th>User Id</th>
                <th>Status</th>
                <th>Followers Count</th>
                <th>Ban</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($pages->currentPage() - 1) * $pages->perPage() + 1; ?>
        @foreach($pages as $page)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{{ $page->page_title }}</td>
                <!-- <td>{{ $page->languages }}</td>
                <td>{{ $page->interests }}</td> -->
                <td><a href="{!! url('/pages_images/'. $page->cover_photo) !!}" target="_blank">
                    <img src="{!! url('/pages_images/'. $page->cover_photo) !!}" width="auto;" height="80"></a>
                </td>
                <td>{{ $page->user_id }}</td>
                <td>{{ $page->status }}</td>
                <td>{{ $page->followers_count }}</td>
                <td>
                @if($page->status == 3)
                <a href="{{ url('/pageban/' . $page->id) }}" class='btn btn-ghost-danger'>
                   <i class="fa fa-check-square" aria-hidden="true"></i></a>
                @endif
                @if($page->status == 1)
                <a href="{{ url('/pageban/' . $page->id) }}" class='btn btn-ghost-danger'>
                <i class="fa fa-ban"></i></a>
                @endif
            </td>
                <td>
                    {!! Form::open(['route' => ['pages.destroy', $page->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pages.show', [$page->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('pages.edit', [$page->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
            <div class="float-right">
                {!! $pages->render() !!}
            </div>
        </div>
</div>