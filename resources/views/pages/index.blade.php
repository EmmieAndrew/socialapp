@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Pages</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <!-- <i class="fa fa-align-justify"></i>
                             Pages -->
                             <div style="text-align: center;">
                                <form action="searchuser" method="get">
                                    <input type="search" style="height: 35px;" class="search" name="search" placeholder="Search..">
                                    <button type="submit" class="btn btn-success button">Search</button>
                                </form>
                                <a class="pull-right" href="{{ route('pages.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                            </div>
                         </div>
                         <div class="card-body">
                             @include('pages.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

