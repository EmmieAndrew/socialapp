<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Query;

class QueryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_query()
    {
        $query = factory(Query::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/queries', $query
        );

        $this->assertApiResponse($query);
    }

    /**
     * @test
     */
    public function test_read_query()
    {
        $query = factory(Query::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/queries/'.$query->id
        );

        $this->assertApiResponse($query->toArray());
    }

    /**
     * @test
     */
    public function test_update_query()
    {
        $query = factory(Query::class)->create();
        $editedQuery = factory(Query::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/queries/'.$query->id,
            $editedQuery
        );

        $this->assertApiResponse($editedQuery);
    }

    /**
     * @test
     */
    public function test_delete_query()
    {
        $query = factory(Query::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/queries/'.$query->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/queries/'.$query->id
        );

        $this->response->assertStatus(404);
    }
}
