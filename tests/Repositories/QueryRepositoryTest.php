<?php namespace Tests\Repositories;

use App\Models\Query;
use App\Repositories\QueryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class QueryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var QueryRepository
     */
    protected $queryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->queryRepo = \App::make(QueryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_query()
    {
        $query = factory(Query::class)->make()->toArray();

        $createdQuery = $this->queryRepo->create($query);

        $createdQuery = $createdQuery->toArray();
        $this->assertArrayHasKey('id', $createdQuery);
        $this->assertNotNull($createdQuery['id'], 'Created Query must have id specified');
        $this->assertNotNull(Query::find($createdQuery['id']), 'Query with given id must be in DB');
        $this->assertModelData($query, $createdQuery);
    }

    /**
     * @test read
     */
    public function test_read_query()
    {
        $query = factory(Query::class)->create();

        $dbQuery = $this->queryRepo->find($query->id);

        $dbQuery = $dbQuery->toArray();
        $this->assertModelData($query->toArray(), $dbQuery);
    }

    /**
     * @test update
     */
    public function test_update_query()
    {
        $query = factory(Query::class)->create();
        $fakeQuery = factory(Query::class)->make()->toArray();

        $updatedQuery = $this->queryRepo->update($fakeQuery, $query->id);

        $this->assertModelData($fakeQuery, $updatedQuery->toArray());
        $dbQuery = $this->queryRepo->find($query->id);
        $this->assertModelData($fakeQuery, $dbQuery->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_query()
    {
        $query = factory(Query::class)->create();

        $resp = $this->queryRepo->delete($query->id);

        $this->assertTrue($resp);
        $this->assertNull(Query::find($query->id), 'Query should not exist in DB');
    }
}
